<?php
namespace Modules\News\Controllers;

use Core\Arr;
use Core\Common;
use Core\Dates;
use Core\HTML;
use Core\Route;
use Core\Text;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\News\Models\News AS Model;
use Modules\Content\Models\Control;

class News extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'TemplateCenterBreadCrumbs';

        $this->_page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $this->_limit = (int)Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $this->_headerImage = is_file(HOST.HTML::media("images/control/{$this->current->image}", false))
            ?
            HTML::media("images/control/{$this->current->image}") : null;
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        Config::set('content_class', 'news_block');
        // Get Rows
        $result = Model::getRows(1, 'date', 'DESC', $this->_limit, $this->_offset)->as_array();
        // Get full count of rows
        $count = Model::countRows(1);
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
		//canonicals settings
		$this->_use_canonical=1;
		$this->_canonical='news';
        // Render template
        $this->_content = View::tpl([
            'result' => $result,
            'pager' => $this->_pager->create(),
            'breadcrumbs' => $this->_breadcrumbs,
            'systemPage' => $this->current,
            '_seo' => $this->_seo
            ], 'News/List');
    }

    public function innerAction()
    {
        $this->_template = 'TemplateTopBreadCrumbs';
        if (Config::get('error')) {
            return false;
        }
        Config::set('content_class', 'new_block');
        // Check for existance
        $obj = Model::getRow(Route::param('alias'), 'alias');
        if (!$obj) {
            return Config::error();
        }
		if ($obj->status != 1) {
			HTTP::redirect('/news',301);
		}
        // Seo
        $this->_seo['h1'] = empty($obj->h1) ? $obj->name : $obj->h1;
        $this->_seo['title'] = $obj->title;
        $this->_seo['keywords'] = $obj->keywords;
        $this->_seo['description'] = $obj->description;
        $this->setBreadcrumbs($obj->name);
        // Add plus one to views
        $obj = Model::addView($obj);
        $obj->date = Dates::generateDateForNews($obj->date);
        // Seo
        $this->setSeoForNewsItem($obj);
        // Include Widgets
        $widgets['prevAndNextItems'] = Widgets::get('News_NextAndPrevItems', ['currentItemID' => $obj->id]);
        $widgets['lastNews'] = Widgets::get('News_LastNews', ['currentItemID' => $obj->id]);
        // Render template
        $this->_content = View::tpl([
            'obj' => $obj,
            'breadcrumbs' => $this->_breadcrumbs,
            'systemPage' => $this->current,
            '_seo' => $this->_seo,
            'widgets' => $widgets
        ], 'News/Inner');
		$this->_content.= View::tpl(['obj' => $obj], 'News/MicroData');
    }

    // Set seo tags from template for items groups
    public function setSeoForNewsItem($item)
    {
        $tpl = Common::factory('seo_templates')->getRow(4);
        $from = ['{{title}}', '{{description}}'];
        $text = trim(strip_tags($item->text));
        $to = [$item->name, $text];
        $res = preg_match_all('/{{description:[0-9]*}}/', $tpl->description, $matches);
        if ($res) {
            $matches = array_unique($matches);
            foreach ($matches[0] AS $pattern) {
                preg_match('/[0-9]+/', $pattern, $m);
                $from[] = $pattern;
                $to[] = Text::limit_words($text, $m[0]);
            }
        }

        empty($item->title) ? $item->title = $item->name : $item->title;
        $title = $item->title ? $item->title : $tpl->title;
        empty($item->h1) ? $item->h1 = $item->title : $item->h1;
        $h1 = $item->h1 ? $item->h1 : $tpl->h1;
        $keywords = $item->keywords ? $item->keywords : $tpl->keywords;
        $description = $item->description ? $item->description : $tpl->description;

        $this->_seo['h1'] = str_replace($from, $to, $h1);
        $this->_seo['title'] = str_replace($from, $to, $title)
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'asc') ? ', От бютжетных к дорогим' : '')
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'desc') ? ', От дорогих к бютжетным' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'desc') ? ', От новых моделей к старым' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'asc') ? ', От старых моделей к новым' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'asc') ? ', По названию от А до Я' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'desc') ? ', По названию от Я до А' : '');
        $this->_seo['keywords'] = str_replace($from, $to, $keywords);
        $this->_seo['description'] = str_replace($from, $to, $description);
        $this->_seo['seo_text'] = $item->text;
        $this->generateParentBreadcrumbs($item->parent_id, 'catalog_tree', 'parent_id', '/products/');
    }

}