<?php
namespace Modules\Ajax\Controllers;

use Core\GeoIP;
use Core\QB\DB;
use Core\Arr;
use Core\User;
use Core\Config AS conf;
use Core\View;
use Core\System;
use Core\Log;
use Core\Email;
use Core\Message;
use Core\Common;
use Modules\Ajax;

class Form extends Ajax
{

    protected $post;
    protected $files;

    function before()
    {
        parent::before();
        // Check for bans in blacklist
        $ip = GeoIP::ip();
        $ips = [];
        $ips[] = $ip;
        $ips[] = $this->ip($ip, [0]);
        $ips[] = $this->ip($ip, [1]);
        $ips[] = $this->ip($ip, [1, 0]);
        $ips[] = $this->ip($ip, [2]);
        $ips[] = $this->ip($ip, [2, 1]);
        $ips[] = $this->ip($ip, [2, 1, 0]);
        $ips[] = $this->ip($ip, [3]);
        $ips[] = $this->ip($ip, [3, 2]);
        $ips[] = $this->ip($ip, [3, 2, 1]);
        $ips[] = $this->ip($ip, [3, 2, 1, 0]);
        if (count($ips)) {
            $bans = DB::select('date')
                ->from('blacklist')
                ->where('status', '=', 1)
                ->where('ip', 'IN', $ips)
                ->and_where_open()
                ->or_where('date', '>', time())
                ->or_where('date', '=', NULL)
                ->and_where_close()
                ->find_all();
            if (sizeof($bans)) {
                $this->error('К сожалению это действие недоступно, т.к. администратор ограничил доступ к сайту с Вашего IP адреса!');
            }
        }
    }

    private function ip($ip, $arr)
    {
        $_ip = explode('.', $ip);
        foreach ($arr AS $pos) {
            $_ip[$pos] = 'x';
        }
        $ip = implode('.', $_ip);
        return $ip;
    }

    // User want to contact with admin and use contact form
    public function contactsAction()
    {
        // Check incoming data
        if (!Arr::get($this->post, 'name')) {
            $this->error('Вы не указали имя!');
        }
        if (!Arr::get($this->post, 'text')) {
            $this->error('Вы не написали текст сообщения!');
        }
        if (!filter_var(Arr::get($this->post, 'phone'))) {
            $this->error('Вы указали не указали номер телефона!');
        }

        // Create data for saving
        $data = [];
        $data['text'] = nl2br(Arr::get($this->post, 'text'));
        $data['ip'] = System::getRealIP();
        $data['name'] = Arr::get($this->post, 'name');
        $data['phone'] = Arr::get($this->post, 'phone');
        $data['created_at'] = time();

        // Chec for bot
        $check = DB::select([DB::expr('COUNT(contacts.id)'), 'count'])
            ->from('contacts')
            ->where('ip', '=', Arr::get($data, 'ip'))
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Нельзя так часто отправлять сообщения! Пожалуйста, повторите попытку через минуту');
        }

        // Save contact message to database
        $keys = [];
        $values = [];
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        $lastID = DB::insert('contacts', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Save log
        $qName = 'Сообщение из контактной формы';
        $url = '/wezom/contacts/edit/' . $lastID;

        $emailData = [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{name}}' =>  Arr::get($data, 'name'),
            '{{phone}}' => Arr::get($data, 'phone'),
            '{{text}}' => Arr::get($data, 'text'),
            '{{ip}}' => Arr::get($data, 'ip'),
            '{{date}}' => date('d.m.Y H:i')
        ];
        // Send E-Mail to admin
        Email::sendTemplate(28, $emailData);

        $this->success('Спасибо за сообщение! Администрация сайта ответит Вам в кратчайшие сроки');
    }



}