<?php

namespace Modules\Contact\Models;

use Core\Common;

class Contact extends Common
{
    public static $table = 'contacts';
}