<?php
namespace Modules\Contact\Controllers;

use Core\GeoIP;
use Core\HTTP;
use Core\Message;
use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control as ControlModel;
use Modules\Contact\Models\Contact as Model;

class Contact extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = ControlModel::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_breadcrumbsOnTop = 1;
        $this->_template = 'TemplateCenterBreadCrumbsWithoutImage';
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $this->_content = View::tpl(['text' => $this->current->text, 'kids' => []], 'Contact/Index');
    }

    public function callbackAction()
    {
        if($_POST){
            $insertData = [];

            $insertData['name'] = $_POST['name'];
            $insertData['phone'] = $_POST['tel'];
            $insertData['text'] = $_POST['message'];
            $insertData['ip'] = GeoIP::ip();

            $res = Model::insert($insertData);
            if($res){
                Message::GetMessage(1, 'Ваше сообшение успешно отправлено');
            } else {
                Message::GetMessage(0, 'Ошибка при отправке сообщения');
            }
            HTTP::redirect('/contact');
        }
        if (Config::get('error')) {
            return false;
        }

        $this->_content = View::tpl([], 'Contact/Callback');
    }

}
    