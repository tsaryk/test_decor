<?php

namespace Modules\Services\Controllers;

use Core\Common;
use Core\HTML;
use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Modules\Base;
use Modules\Services\Models\Services AS Model;
use Modules\Content\Models\Control;

class Services extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'TemplateCenterBreadCrumbs';
        $this->_headerImage = is_file(HOST.HTML::media("images/control/{$this->current->image}", false))
            ?
            HTML::media("images/control/{$this->current->image}") : null;
        $this->_breadcrumbsOnTop = 0;
        $this->_showHeadSection = 1;
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Get Rows
        $services = Model::getRows(1)->as_array();
        // get head rows
        $this->_seo['breadcrumbs'] = $this->_breadcrumbs;
        // Render template
        $this->_content = View::tpl([
            'services' => empty($services) || $services == null ? null : $services,
            '_seo' => $this->_seo,
            'systemPage' => $this->current
        ], 'Services/List');
    }
}