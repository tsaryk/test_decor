<?php

namespace Modules\Services\Models;

use Core\Common;

class Services extends Common
{
    public static $table = 'services';
    public static $image = 'services';
}