<?php

return [
    'services' => 'services/services/index',
    'services/<alias:[a-zA-Z0-9_-]*>' => 'services/services/page'
];