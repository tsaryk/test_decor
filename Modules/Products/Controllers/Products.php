<?php

namespace Modules\Products\Controllers;

use Core\Common;
use Core\Config;
use Core\HTML;
use Core\Pager\Pager;
use Core\Route;
use Core\View;
use Modules\Base;
use Wezom\Modules\Content\Models\Control;

use Modules\Products\Models\Products as Model;

class Products extends Base
{
    public $tpl_folder = 'Products';
    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'TemplateCenterBreadCrumbs';
        $this->_headerImage = is_file(HOST.HTML::media("images/control/{$this->current->image}", false))
            ?
            HTML::media("images/control/{$this->current->image}") : null;
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->name;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->text;
        $this->_seo['breadcrumbs'] = $this->_breadcrumbs;

        $result = Model::getRows(1)->as_array();
        //canonicals settings
        $this->_use_canonical=1;
        $this->_canonical='products';

        $this->_content = View::tpl([
            'result' => $result,
            '_seo' => $this->_seo,
            'systemPage' => $this->current
        ],
            "{$this->tpl_folder}/List");
    }
}