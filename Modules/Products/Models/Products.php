<?php

namespace Modules\Products\Models;

use Core\Common;

class Products extends Common
{
    public static $table = 'products';
    public static $image = 'products';
}