<?php

namespace Modules\Portfolio\Controllers;

use Core\Arr;
use Core\Common;
use Core\HTML;
use Core\HTTP;
use Core\Pager\Pager;
use Core\Route;
use Core\Config;
use Core\Text;
use Modules\Base;
use Modules\Content\Models\Control;
use Core\View;
use Modules\Portfolio\Models\Portfolio as Model;
use stdClass;

class Portfolio extends Base
{
    public $tpl_folder = 'Portfolio';
    public $_breadcrumbs;
    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs('Портфолио', Route::controller());
        $this->page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $this->_limit = (int)Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $this->_template = 'TemplateCenterBreadCrumbs';
        $this->_headerImage = is_file(HOST.HTML::media("images/control/{$this->current->image}", false))
            ?
            HTML::media("images/control/{$this->current->image}") : null;
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $portfolio = Model::getGroups()->as_array();

        $this->_content = View::tpl([
            'portfolio' => $portfolio,
            'systemPage' => $this->current,
            '_seo' => $this->_seo

        ], "{$this->tpl_folder}/List");
    }

    public function groupAction()
    {
        $alias = Route::param('alias');
        $group = Model::getRow($alias, 'alias');

        $allPages = Model::getPagesOfGroup($group->id)->as_array();

        if(!$group){
            return Config::error();
        }

        if ($group->status != 1) {
            HTTP::redirect('/', 301);
        }
        // Seo
        $this->setSeoForPortfolioItem($group);
        // Breadcrumbs
        $this->setBreadcrumbs($group->name, "{$group->alias}");

        // Seo
        $this->_seo['h1'] = !empty($this->_seo['h1']) ? $group->name : $this->_seo['h1'];
        $this->_seo['title'] = $this->current->name;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        // Render template
        $this->_content = View::tpl([
            'group' => $group,
            'result' => $allPages,
            '_seo' => $this->_seo,
            'systemPage' => $this->current
        ], "{$this->tpl_folder}/Group");
    }

    public function pageAction()
    {
        $groupAlias = Route::param('group');
        $pageAlias = Route::param('page');

        $group = Model::getRow($groupAlias, 'alias');
        $page = Model::getRow($pageAlias, 'alias');

        $page->images = Model::getPageImages($page->id);

        if(!$group || !$page){
            return Config::error();
        }

        if ($group->status != 1 || $page->status != 1) {
            HTTP::redirect('/', 301);
        }
        // Seo
        $obj = $this->prepareDataForSeo($group, $page);

        $this->setSeoForPortfolioItem($obj);

        $this->setBreadcrumbs($group->name, "portfolio/".$group->alias);
        $this->setBreadcrumbs($page->name);

        $this->_seo['h1'] = !empty($this->_seo['h1']) ? $obj->title : $this->_seo['h1'];

        // Render template
        $this->_content = View::tpl([
            'result' => $page,
            'systemPage' => $this->current,
            '_seo' => $this->_seo
        ], "{$this->tpl_folder}/Page");
    }

    public function prepareDataForSeo($group, $page)
    {
        $newObj = new stdClass();

        $newObj->title = isset($page) ? "{$page->name} - {$group->name}" : null;
        $newObj->description = isset($page) ? Text::limit_words($page->text, 255) : null;
        $newObj->keywords = isset($page) ? $page->keywords : null;
        $newObj->h1 = isset($page) ? $page->h1 : null;
        $newObj->image = isset($page) ? $page->image : null;

        return $newObj;
    }

    // Set seo tags from template for items groups
    public function setSeoForPortfolioItem($item)
    {
        $tpl = Common::factory('seo_templates')->getRow(5);
        $from = ['{{title}}', '{{description}}'];
        $text = trim(strip_tags($item->text));
        $to = [$item->name, $text];
        $res = preg_match_all('/{{description:[0-9]*}}/', $tpl->description, $matches);
        if ($res) {
            $matches = array_unique($matches);
            foreach ($matches[0] AS $pattern) {
                preg_match('/[0-9]+/', $pattern, $m);
                $from[] = $pattern;
                $to[] = Text::limit_words($text, $m[0]);
            }
        }

        $title = $item->title ? $item->title : $tpl->title;
        $h1 = $item->h1 ? $item->h1 : $tpl->h1;
        $keywords = $item->keywords ? $item->keywords : $tpl->keywords;
        $description = $item->description ? $item->description : $tpl->description;

        $this->_seo['h1'] = str_replace($from, $to, $h1);
        $this->_seo['title'] = str_replace($from, $to, $title)
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'asc') ? ', От бютжетных к дорогим' : '')
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'desc') ? ', От дорогих к бютжетным' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'desc') ? ', От новых моделей к старым' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'asc') ? ', От старых моделей к новым' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'asc') ? ', По названию от А до Я' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'desc') ? ', По названию от Я до А' : '');
        $this->_seo['keywords'] = str_replace($from, $to, $keywords);
        $this->_seo['description'] = str_replace($from, $to, $description);
        $this->_seo['seo_text'] = $item->text;
        $this->generateParentBreadcrumbs($item->parent_id, 'portfolio', 'parent_id', '/portfolio/');
//        $this->setBreadcrumbs($item->name);
    }

}