<?php

return [
    'portfolio' => 'portfolio/portfolio/index',
    'portfolio/<alias:[a-zA-Z0-9-_]*>' => 'portfolio/portfolio/group',
    'portfolio/<group:[a-zA-Z0-9-_]*>/<page:[a-zA-Z0-9-_]*>' => 'portfolio/portfolio/page',
];