<?php

namespace Modules\Portfolio\Models;

use Core\Common;
use Core\Config;
use Core\QB\DB;

class Portfolio extends Common
{
    public static $table = 'portfolio';
    public static $image = 'portfolio';

    public static function getPagesOfGroup($groupID)
    {
        if(empty($groupID) || !is_numeric($groupID)){
            return Config::error();
        }

        $result = DB::select()->from(static::$table)->where('parent_id', '=', $groupID);

        return $result->find_all();
    }

    public static function getPageImages($pageID)
    {
        if(empty($pageID) || !is_numeric($pageID)){
            return Config::error();
        }

        $result = DB::select()->from('portfolio_images')->where('portfolio_id', '=', $pageID);

        return $result->find_all();
    }

    public static function getGroups()
    {
        $result = DB::select()->from(self::$table)
            ->where('parent_id', '=', '0')
            ->where('status', '=', 1)
            ->order_by('id', 'DESC');

        return $result->find_all();
    }
}