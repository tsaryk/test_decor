-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: decor_db
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `controller` varchar(64) DEFAULT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`) USING BTREE,
  CONSTRAINT `access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES (2,5,'index',1,0),(3,5,'content',0,0),(4,5,'control',0,0),(5,5,'news',0,0),(6,5,'articles',1,0),(7,5,'menu',0,0),(8,5,'slider',0,0),(9,5,'gallery',0,0),(10,5,'banners',0,0),(11,5,'questions',0,0),(12,5,'comments',0,0),(13,5,'groups',0,0),(14,5,'items',0,0),(15,5,'brands',0,0),(16,5,'models',0,0),(17,5,'specifications',0,0),(18,5,'orders',0,0),(19,5,'simple',0,0),(20,5,'users',0,0),(21,5,'subscribers',0,0),(22,5,'subscribe',0,0),(23,5,'contacts',0,0),(24,5,'callback',0,0),(25,5,'mailTemplates',0,0),(26,5,'config',0,0),(27,5,'templates',0,0),(28,5,'links',0,0),(29,5,'scripts',0,0),(30,5,'redirects',0,0),(31,6,'button',0,0),(32,6,'index',0,0),(33,6,'content',0,0),(34,6,'control',0,0),(35,6,'news',0,0),(36,6,'articles',0,0),(37,6,'menu',0,0),(38,6,'slider',0,0),(39,6,'gallery',0,0),(40,6,'banners',0,0),(41,6,'questions',0,0),(42,6,'comments',0,0),(43,6,'groups',0,0),(44,6,'items',0,0),(45,6,'brands',0,0),(46,6,'models',0,0),(47,6,'specifications',0,0),(48,6,'orders',0,0),(49,6,'simple',0,0),(50,6,'users',0,0),(51,6,'subscribers',0,0),(52,6,'subscribe',0,0),(53,6,'contacts',0,0),(54,6,'callback',0,0),(55,6,'mailTemplates',0,0),(56,6,'config',0,0),(57,6,'templates',0,0),(58,6,'links',0,0),(59,6,'scripts',0,0),(60,6,'redirects',0,0),(121,7,'button',0,0),(122,7,'index',1,0),(123,7,'content',1,1),(124,7,'control',1,1),(125,7,'news',1,1),(126,7,'articles',1,1),(127,7,'menu',1,1),(128,7,'slider',1,1),(129,7,'gallery',1,1),(130,7,'banners',1,1),(131,7,'questions',1,1),(132,7,'comments',1,1),(133,7,'groups',1,1),(134,7,'items',1,1),(135,7,'brands',1,1),(136,7,'models',1,1),(137,7,'specifications',1,1),(138,7,'orders',1,1),(139,7,'simple',1,1),(140,7,'users',1,1),(141,7,'subscribers',1,1),(142,7,'subscribe',1,1),(143,7,'contacts',1,1),(144,7,'callback',1,1),(145,7,'mailTemplates',1,1),(146,7,'config',1,1),(147,7,'templates',1,1),(148,7,'links',1,1),(149,7,'scripts',1,1),(150,7,'redirects',1,1);
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blacklist`
--

LOCK TABLES `blacklist` WRITE;
/*!40000 ALTER TABLE `blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `blacklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `alias` varchar(255) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `views` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (47,1496831837,NULL,1,0,'Производитель','','proizvoditel','','','','',0,NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `callback`
--

DROP TABLE IF EXISTS `callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `callback`
--

LOCK TABLES `callback` WRITE;
/*!40000 ALTER TABLE `callback` DISABLE KEYS */;
INSERT INTO `callback` VALUES (1,1448259837,NULL,1,'dfsd','+38 (555) 555-55-55','178.136.229.251'),(2,1453192341,NULL,1,'Darya','+38 (564) 654-56-46','178.136.229.251'),(3,1453375180,NULL,1,'Darya','+38 (564) 654-56-46','178.136.229.251'),(4,1453375180,NULL,1,'Darya','+38 (564) 654-56-46','178.136.229.251'),(5,1453375428,NULL,1,'Darya','+38 (564) 654-56-46','178.136.229.251'),(6,1460985084,NULL,1,'hhh','+38 (777) 777-77-77','178.136.229.251'),(7,1461527997,1483518966,1,'test','+38 (095) 106-66-07','213.227.252.130'),(8,1493295046,NULL,0,'sdfsdf','+38 (234) 324-32-42','127.0.0.1'),(9,1493298079,NULL,0,'sdfsdfsd','+38 (324) 234-23-42','127.0.0.1');
/*!40000 ALTER TABLE `callback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102040 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` VALUES (102039,1497244217,NULL,'5f3195993d41240aee355ea97c00e47dd7768ac7');
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts_items`
--

DROP TABLE IF EXISTS `carts_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carts_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) NOT NULL,
  `catalog_id` int(10) NOT NULL,
  `count` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cart` (`cart_id`) USING BTREE,
  KEY `id_goods` (`catalog_id`) USING BTREE,
  KEY `cart_id` (`cart_id`) USING BTREE,
  KEY `catalog_id` (`catalog_id`) USING BTREE,
  CONSTRAINT `carts_items_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `carts_items_ibfk_2` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts_items`
--

LOCK TABLES `carts_items` WRITE;
/*!40000 ALTER TABLE `carts_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `carts_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `cost` int(6) NOT NULL DEFAULT '0',
  `cost_old` int(6) NOT NULL DEFAULT '0',
  `artikul` varchar(128) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  `brand_alias` varchar(255) DEFAULT NULL,
  `model_alias` varchar(255) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `specifications` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `cat_brand_alias` (`brand_alias`) USING BTREE,
  KEY `cat_model_alias` (`model_alias`) USING BTREE,
  KEY `image_link` (`image`) USING BTREE,
  CONSTRAINT `catalog_ibfk_1` FOREIGN KEY (`brand_alias`) REFERENCES `brands` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `catalog_ibfk_2` FOREIGN KEY (`model_alias`) REFERENCES `models` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `catalog_ibfk_3` FOREIGN KEY (`image`) REFERENCES `catalog_images` (`image`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `catalog_ibfk_4` FOREIGN KEY (`parent_id`) REFERENCES `catalog_tree` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (167,1496832236,1496836026,1,0,'caparol','caparol',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире. Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'6f4777272d904980f2950d4ba232eb8d.jpg',NULL),(168,1496835614,1496835678,1,2,'TEKNOS','teknos',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире. Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'7cf6faf5ab4a6d55146792ece2154e30.jpg',NULL),(169,1496835640,1496835668,1,3,'FERRARA PAINT','ferrara-paint',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'27a012bbfb1be25fe9e75ea3bf4ae47d.jpg',NULL),(170,1496835742,1496835959,1,4,'KOLORIT','kolorit',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'b8f4254a27ed6d3c35030b83cb67349c.jpg',NULL),(171,1496835780,1496835958,1,5,'TIKKURILA','tikkurila',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'d3f9a772bbef55e6d0ec7b29548cb0c8.jpg',NULL),(172,1496835820,1496835958,1,6,'SILTEK','siltek',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'0867b257980c1c13f4bcb10c535a1ba2.jpg',NULL),(173,1496835845,1496835956,1,7,'KRAUTOL','krautol',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'bee3b103a47e2b0cf8da12395bcb9f70.jpg',NULL),(176,1496835993,1496836030,1,8,'NOVACOLOR','novacolor',55,0,0,0,1,'','','','Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.',1,0,'',0,NULL,NULL,'0fc876199ae6b3a2b4ba2062f9b3b0cd.jpg',NULL);
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_comments`
--

DROP TABLE IF EXISTS `catalog_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` int(10) DEFAULT NULL,
  `catalog_id` int(10) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `text` text,
  `ip` varchar(16) DEFAULT NULL,
  `rate` double(2,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_id` (`catalog_id`) USING BTREE,
  CONSTRAINT `catalog_comments_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_comments`
--

LOCK TABLES `catalog_comments` WRITE;
/*!40000 ALTER TABLE `catalog_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_images`
--

DROP TABLE IF EXISTS `catalog_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(10) NOT NULL DEFAULT '0',
  `catalog_id` int(10) NOT NULL DEFAULT '0',
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_id` (`catalog_id`) USING BTREE,
  KEY `image` (`image`) USING BTREE,
  CONSTRAINT `catalog_images_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_images`
--

LOCK TABLES `catalog_images` WRITE;
/*!40000 ALTER TABLE `catalog_images` DISABLE KEYS */;
INSERT INTO `catalog_images` VALUES (1099,1496832262,NULL,0,167,1,'6f4777272d904980f2950d4ba232eb8d.jpg'),(1100,1496832262,NULL,0,167,0,'5308917d33b3081c081d4cda9b693cac.jpg'),(1101,1496835653,NULL,0,169,1,'27a012bbfb1be25fe9e75ea3bf4ae47d.jpg'),(1102,1496835653,NULL,0,169,0,'c5ee914692f5f33a2871362188bdf9ef.jpg'),(1103,1496835678,NULL,0,168,1,'7cf6faf5ab4a6d55146792ece2154e30.jpg'),(1104,1496835678,NULL,0,168,0,'56243a3ccfdeb1a68dacd655cb9d9cbe.jpg'),(1105,1496835750,NULL,0,170,1,'b8f4254a27ed6d3c35030b83cb67349c.jpg'),(1106,1496835750,NULL,0,170,0,'18861003f7091772396907408b236b38.jpg'),(1107,1496835793,NULL,0,171,1,'d3f9a772bbef55e6d0ec7b29548cb0c8.jpg'),(1108,1496835793,NULL,0,171,0,'2ff6ad9846de1ed90ea1f98d04799470.jpg'),(1111,1496835911,NULL,0,172,0,'cdb34629ce3c40f6cee579dacb1ce16e.jpg'),(1112,1496835911,1496835915,0,172,1,'0867b257980c1c13f4bcb10c535a1ba2.jpg'),(1113,1496835936,NULL,0,173,1,'bee3b103a47e2b0cf8da12395bcb9f70.jpg'),(1114,1496835936,NULL,0,173,0,'d38d1c0018c5fa850b4fced420ea673f.jpg'),(1115,1496836001,NULL,0,176,0,'923f53f053fa81e755226c6f8845fc9a.jpg'),(1116,1496836001,1496836005,0,176,1,'0fc876199ae6b3a2b4ba2062f9b3b0cd.jpg');
/*!40000 ALTER TABLE `catalog_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_questions`
--

DROP TABLE IF EXISTS `catalog_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `text` text,
  `catalog_id` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_id` (`catalog_id`) USING BTREE,
  CONSTRAINT `catalog_questions_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_questions`
--

LOCK TABLES `catalog_questions` WRITE;
/*!40000 ALTER TABLE `catalog_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_related`
--

DROP TABLE IF EXISTS `catalog_related`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_related` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `who_id` int(10) NOT NULL,
  `with_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_id` (`who_id`) USING BTREE,
  KEY `related_id` (`with_id`) USING BTREE,
  CONSTRAINT `catalog_related_ibfk_1` FOREIGN KEY (`who_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `catalog_related_ibfk_2` FOREIGN KEY (`with_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_related`
--

LOCK TABLES `catalog_related` WRITE;
/*!40000 ALTER TABLE `catalog_related` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_related` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_specifications_values`
--

DROP TABLE IF EXISTS `catalog_specifications_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_specifications_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalog_id` int(10) NOT NULL,
  `specification_value_alias` varchar(255) NOT NULL,
  `specification_alias` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_id` (`catalog_id`) USING BTREE,
  KEY `spec_alias` (`specification_alias`) USING BTREE,
  KEY `spec_val_alias` (`specification_value_alias`) USING BTREE,
  CONSTRAINT `catalog_specifications_values_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `catalog_specifications_values_ibfk_2` FOREIGN KEY (`specification_alias`) REFERENCES `specifications` (`alias`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `catalog_specifications_values_ibfk_3` FOREIGN KEY (`specification_value_alias`) REFERENCES `specifications_values` (`alias`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_specifications_values`
--

LOCK TABLES `catalog_specifications_values` WRITE;
/*!40000 ALTER TABLE `catalog_specifications_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_specifications_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_tree`
--

DROP TABLE IF EXISTS `catalog_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_tree` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `image` varchar(64) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `views` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_tree`
--

LOCK TABLES `catalog_tree` WRITE;
/*!40000 ALTER TABLE `catalog_tree` DISABLE KEYS */;
INSERT INTO `catalog_tree` VALUES (55,1496831742,NULL,1,0,'Тест','test',0,NULL,'','','','','',0);
/*!40000 ALTER TABLE `catalog_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_tree_brands`
--

DROP TABLE IF EXISTS `catalog_tree_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_tree_brands` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) NOT NULL,
  `catalog_tree_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`) USING BTREE,
  KEY `catalog_tree_id` (`catalog_tree_id`) USING BTREE,
  CONSTRAINT `catalog_tree_brands_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `catalog_tree_brands_ibfk_2` FOREIGN KEY (`catalog_tree_id`) REFERENCES `catalog_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_tree_brands`
--

LOCK TABLES `catalog_tree_brands` WRITE;
/*!40000 ALTER TABLE `catalog_tree_brands` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_tree_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_tree_specifications`
--

DROP TABLE IF EXISTS `catalog_tree_specifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_tree_specifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `catalog_tree_id` int(10) NOT NULL,
  `specification_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_tree_id` (`catalog_tree_id`) USING BTREE,
  KEY `specification_id` (`specification_id`) USING BTREE,
  CONSTRAINT `catalog_tree_specifications_ibfk_1` FOREIGN KEY (`catalog_tree_id`) REFERENCES `catalog_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `catalog_tree_specifications_ibfk_2` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_tree_specifications`
--

LOCK TABLES `catalog_tree_specifications` WRITE;
/*!40000 ALTER TABLE `catalog_tree_specifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_tree_specifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `zna` text,
  `updated_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(32) DEFAULT NULL,
  `values` text COMMENT 'Возможные значения в json массиве ключ => значение. Для селекта и радио',
  `group` varchar(128) DEFAULT NULL COMMENT 'Группа настроек',
  PRIMARY KEY (`id`),
  KEY `block` (`group`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  CONSTRAINT `config_ibfk_1` FOREIGN KEY (`group`) REFERENCES `config_groups` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `config_ibfk_2` FOREIGN KEY (`type`) REFERENCES `config_types` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'E-Mail администратора сайта (отправитель по умолчанию)','tsaryk.a.wezom@gmail.com',1434885560,1,1,'admin_email',1,'input',NULL,'mail'),(2,'Название сайта','CMS v4.0',1434885560,1,1,'name_site',1,'input',NULL,'basic'),(4,'Copyright','© 2017. Эксперт красок.  All right reserved',1434885560,1,4,'copyright',1,'input',NULL,'static'),(5,'Отображение всплывающих сообщений на сайте','topRight',1434835221,0,2000,'message_output',1,'radio','[{\"key\":\"Отображать вверху\",\"value\":\"top\"},{\"key\":\"Отображать вверху слева\",\"value\":\"topLeft\"},{\"key\":\"Отображать вверху по центру\",\"value\":\"topCenter\"},{\"key\":\"Отображать вверху справа\",\"value\":\"topRight\"},{\"key\":\"Отображать по центру слева\",\"value\":\"centerLeft\"},{\"key\":\"Отображать по центру\",\"value\":\"center\"},{\"key\":\"Отображать по центру справа\",\"value\":\"centerRight\"},{\"key\":\"Отображать внизу слева\",\"value\":\"bottomLeft\"},{\"key\":\"Отображать внизу по центру\",\"value\":\"bottomCenter\"},{\"key\":\"Отображать внизу справа\",\"value\":\"bottomRight\"},{\"key\":\"Отображать внизу\",\"value\":\"bottom\"}]','basic'),(6,'Номер телефона в шапке сайта','067 700 23 70',1434885560,1,6,'phone',0,'input',NULL,'static'),(7,'Количество товаров на странице','15',1434885560,1,7,'limit',1,'input',NULL,'basic'),(8,'Количество статей на главной странице','10',1434885560,1,8,'limit_articles_main_page',1,'input',NULL,'basic'),(9,'Количество строк в админ-панели','10',1434885560,1,9,'limit_backend',1,'input',NULL,'basic'),(10,'VK.com','',1434885560,1,10,'vk',0,'input',NULL,'socials'),(11,'FB.com','https://www.facebook.com',1434885560,1,11,'fb',0,'input',NULL,'socials'),(12,'Instagram','',1434885560,1,12,'instagram',0,'input',NULL,'socials'),(13,'Надпись в подвале сайта для подписчиков','Хочешь быть в числе первых, кому мы сообщим об акциях и новинках?!',1434885560,1,5,'subscribe_text',0,'input',NULL,'static'),(17,'Количество новостей / статей на странице','10',1434885560,1,7,'limit_articles',1,'input',NULL,'basic'),(18,'Количество групп товаров на странице','5',1434885560,1,7,'limit_groups',1,'input',NULL,'basic'),(19,'Использовать СМТП','1',1434885560,1,3,'smtp',1,'radio','[{\"key\":\"Да\",\"value\":1},{\"key\":\"Нет\",\"value\":0}]','mail'),(20,'SMTP server','smtp.gmail.com',1434885560,1,4,'host',0,'input',NULL,'mail'),(22,'Логин','tsaryk.a.wezom@gmail.com',1434885560,1,5,'username',0,'input',NULL,'mail'),(23,'Пароль','123456Ta',1434885560,1,6,'pass',0,'password',NULL,'mail'),(24,'Тип подключения','tls',1434885560,1,7,'secure',0,'select','[{\"key\":\"TLS\",\"value\":\"tls\"},{\"key\":\"SSL\",\"value\":\"ssl\"}]','mail'),(25,'Порт. Например 465 или 587. (587 по умолчанию)','587',1434885560,1,8,'port',0,'input',NULL,'mail'),(26,'Имя латинницей (отображается в заголовке письма)','Info',1434885560,1,2,'name',1,'input',NULL,'mail'),(27,'Запаролить сайт','0',1434885560,1,0,'auth',1,'radio','[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]','security'),(28,'Логин','1',1434885560,1,2,'username',0,'input',NULL,'security'),(29,'Пароль','1',1434885560,1,3,'password',0,'password',NULL,'security'),(30,'Сократить CSS u JavaScript','0',1434885561,1,1,'minify',1,'radio','[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]','speed'),(31,'Сократить HTML','0',1434885561,1,2,'compress',1,'radio','[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]','speed'),(32,'Кеширование размера изображений','0',NULL,1,3,'cache_images',1,'select','[{\"key\":\"Выключить\",\"value\":\"0\"},{\"key\":\"12 часов\",\"value\":\"0.5\"},{\"key\":\"День\",\"value\":\"1\"},{\"key\":\"3 дня\",\"value\":\"3\"},{\"key\":\"Неделя\",\"value\":\"7\"},{\"key\":\"2 недели\",\"value\":\"14\"},{\"key\":\"Месяц\",\"value\":\"30\"},{\"key\":\"Год\",\"value\":\"365\"}]','speed'),(33,'Наименование организации','wezom',NULL,1,1,'organization',1,'input',NULL,'microdata'),(34,'Автор статей','wezom author',NULL,1,2,'author',1,'input ',NULL,'microdata'),(35,'Количество отзывов на странице','10',NULL,1,8,'limit_reviews',1,'input',NULL,'basic'),(36,'Наименование магазина','cms4',NULL,1,1,'name',1,'input',NULL,'export'),(37,'Наименование компании','wezom',NULL,1,2,'company',1,'input',NULL,'export'),(38,'Email разработчиков CMS или агентства, осуществляющего техподдержку.','alyohina.i.wezom@gmail.com',NULL,1,3,'email',1,'input',NULL,'export'),(40,'Использовать защищённое ssl соединение?','0',NULL,1,5,'ssl',1,'radio','[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]','security'),(44,'Номер телефона в шапке сайта','095 605 67 38',NULL,1,8,'second_phone',0,'input',NULL,'static'),(45,'Номер телефона в футере сайта','0951234567',NULL,1,9,'thrird_phone',0,'input',NULL,'static'),(46,'E-mail в подвале сайта','tikkurila.kherson@gmail.com',NULL,1,10,'footer_email',0,'input',NULL,'static'),(47,'Адрес в подвале сайта','ХЕРСОН, УЛ. СТРЕТЕНСКАЯ, 11-Б',NULL,1,11,'footer_address',0,'input',NULL,'static'),(48,'Левый блок, текст сверху','более',NULL,1,1,'left_top',0,'input',NULL,'achievements'),(49,'Левый блок, количетсво','11',NULL,1,2,'left_center',0,'input',NULL,'achievements'),(50,'Левый блок, текст снизу','товаров в ассортименте',NULL,1,3,'left_down',0,'input',NULL,'achievements'),(51,'Блок по-центру, текст сверху','тонировка в более',NULL,1,4,'center_top',0,'input',NULL,'achievements'),(52,'Блок по-центру, количетсво','12',NULL,1,5,'center_block',0,'input',NULL,'achievements'),(53,'Блок по-центру, текст снизу','оттенков',NULL,1,6,'center_down',0,'input',NULL,'achievements'),(54,'Правый блок, текст сверху','более',NULL,1,7,'right_top',0,'input',NULL,'achievements'),(55,'Правый блок, количетсво','13',NULL,1,8,'right_center',0,'input',NULL,'achievements'),(56,'Правый блок, текст снизу','реализованных объектов',NULL,1,9,'right_down',0,'input',NULL,'achievements'),(57,'Карта на странице контакты','<script src=\'https://maps.googleapis.com/maps/api/js?key=AIzaSyDqj4fG4-WBkEp4VwztoDUy4IpN55ju-q4&callback=initMap\'></script>',NULL,1,20,'contact_map',0,'textarea',NULL,'static');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_groups`
--

DROP TABLE IF EXISTS `config_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_groups` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `side` varchar(16) NOT NULL DEFAULT 'left' COMMENT 'left, right',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_groups`
--

LOCK TABLES `config_groups` WRITE;
/*!40000 ALTER TABLE `config_groups` DISABLE KEYS */;
INSERT INTO `config_groups` VALUES (1,'Почта','mail','right',1,1),(2,'Базовые','basic','left',1,1),(3,'Статика','static','left',1,2),(4,'Соц. сети','socials','left',1,3),(5,'Безопасность','security','right',1,2),(6,'Быстродействие','speed','right',1,3),(7,'Микроразметка','microdata','left',1,4),(8,'Выгрузки','export','left',1,5),(9,'Блок достижений','achievements','left',1,6);
/*!40000 ALTER TABLE `config_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_types`
--

DROP TABLE IF EXISTS `config_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_types` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_types`
--

LOCK TABLES `config_types` WRITE;
/*!40000 ALTER TABLE `config_types` DISABLE KEYS */;
INSERT INTO `config_types` VALUES (1,'Однострочное текстовое поле','input'),(2,'Текстовое поле','textarea'),(3,'Выбор значения из списка','select'),(4,'Пароль','password'),(5,'Радиокнопка','radio'),(6,'Текстовое поле с редактором','tiny');
/*!40000 ALTER TABLE `config_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `text` text,
  `ip` varchar(16) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,1497510145,NULL,0,'Александр',NULL,'dfzxcvefrdvszgdsfgdsfg','127.0.0.1','+38(095)6343602'),(2,1497514308,NULL,0,'Александр',NULL,'sadfzxcvzxcvdsasdfxzcvz','127.0.0.1','+38(012)3465789'),(3,1497514774,NULL,0,'test',NULL,'sadfvzxcverfsdfasdf','127.0.0.1','+38(000)0000000'),(4,1497514877,NULL,0,'test',NULL,'testtesttest','127.0.0.1','+38(000)0000000'),(5,1497514988,NULL,0,'13',NULL,'11111111111111111111111111111','127.0.0.1','+38(011)1111111'),(6,1497515155,NULL,0,'13',NULL,'123123123123123123','127.0.0.1','+38(011)1111111'),(7,1497515255,NULL,0,'13',NULL,'123123123123123123','127.0.0.1','+38(011)1111111'),(8,1497515318,NULL,0,'13',NULL,'123123123123123','127.0.0.1','+38(011)1111111'),(9,1497515554,NULL,0,'13',NULL,'123123123123123','127.0.0.1','+38(011)1111111'),(10,1497515614,NULL,0,'13',NULL,'123123123123123','127.0.0.1','+38(011)1111111'),(11,1497515806,NULL,0,'13',NULL,'123123123123123','127.0.0.1','+38(011)1111111'),(12,1497515866,NULL,0,'13',NULL,'12312432423r3145213451','127.0.0.1','+38(011)1111111');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `alias` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `title` text CHARACTER SET cp1251,
  `description` text CHARACTER SET cp1251,
  `keywords` text CHARACTER SET cp1251,
  `text` text CHARACTER SET cp1251,
  `status` int(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `h1` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `class` varchar(64) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (2,'Наши услуги','nashi-uslugi','','','','',1,1496900915,'',1496900917,0,0,NULL,0),(3,'Вопросы и ответы','voprosy-i-otvety','','','','',1,1496900926,'',NULL,0,2,NULL,0);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control`
--

DROP TABLE IF EXISTS `control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `alias` varchar(32) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `other` text,
  `updated_at` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control`
--

LOCK TABLES `control` WRITE;
/*!40000 ALTER TABLE `control` DISABLE KEYS */;
INSERT INTO `control` VALUES (1,'Главная страница','Эксперт красок! - продажа лакокрасочной продукции, декоративных штукатурок, выполнение всех видов строительных работ.','Эксперт красок! - продажа лакокрасочной продукции, декоративных штукатурок, выполнение всех видов строительных работ.','Эксперт красок! - продажа лакокрасочной продукции, декоративных штукатурок, выполнение всех видов строительных работ.','Эксперт красок! - продажа лакокрасочной продукции, декоративных штукатурок, выполнение всех видов строительных работ.','<h3>Уважаемые клиенты!!!&nbsp;</h3>\r\n<p>&nbsp;Предлагаем вам приблизительные расценки на ремонт квартир в Киеве, чтобы Вам было легче сориентироваться в предварительной стоимости ремонтных работ.</p>\r\n<p>Ремонт квартир под ключ (комплексный) - это высококачественная отделка с использованием полифункциональных материалов и современных технологий. Она включает комплекс работ, направленных на удаление прежней отделки, абсолютное выравнивание несущих конструкций (стен, пола, потолка), замену инженерных коммуникаций, перепланировку. Цена за комплексный ремонт квартиры, офиса, дома составляет от 800 грн за 1 м.кв по полу.</p>\r\n<p>Косметический ремонт квартир - в процессе мелкого ремонта квартиры вы можете улучшить вашу жилую площадь в кротчайшие сроки. А сумма, вложенная в ремонт окажется очень даже выгодной. Косметический ремонт - это в первую очередь замена обоев, линолеума, шпаклевка и покраска потолков, поклейка багетов, замена розеток .Цена за косметический ремонт квартиры составляет от 500 грн за 1 м.кв по полу.</p>\r\n<p>Ремонт с нами - это выгодно, качественно и надежно!&nbsp;<br />На наш ремонт квартир Херсона цены самые доступные. Обратившись к нам, Вы получаете ремонт квартиры недорого при этом высокое качество обслуживания, смета, договор и профессиональный подход к работе. Мы всегда будем рады видеть Вас среди наших клиентов!</p>','index',1,NULL,1497331898,'9dbebd00899690b91c5ca9905af1e311.jpg'),(2,'Контакты','контактная информация','контактная информация','контактная информация','контактная информация','<p><strong>Контактная информация компании Интернет-магазин \"Airpac\"</strong></p>\r\n<p><br />Название:&nbsp;&nbsp; &nbsp;Интернет-магазин одежды и обуви<br />Контактное лицо:&nbsp;&nbsp;&nbsp; Администратор<br />Адрес:&nbsp;&nbsp; &nbsp;Только online-магазин, Украина<br />Телефон:&nbsp;&nbsp; &nbsp;<br />+38(XXX)XXX-XX-XX<br />+38(XXX)XXX-XX-XX<br />Email:&nbsp;&nbsp;&nbsp; <a href=\"mailto:office@wezom.com.u\">office@wezom.com.ua</a></p>','contact',1,'',1497428005,NULL),(15,'Продукция','h1 products 123','','','','<blockquote>Декоративное покрытие GROTTO, созданное на основе натуральной извести, сегодня широко используется для декорирования интерьеров и фасадов. Это уникальная возможность создания собственного неповторимого интерьера с различными декоративными решениями. Благодаря особому составу можно создать многопрофильные покрытия &ndash; от грубой &laquo;венецианской штукатурки&raquo; до фактурного рельефа. Применение дополнительных лаков позволит покрытию великолепно смотреться в современных интерьерах, а умелое сочетание покрытия Grotto с декоративным покрытием Murano создаст поистине неповторимую красоту убранства.</blockquote>\r\n<div class=\"products__head-img\"><img src=\"http://decor.project/Media/files/filemanager/products_big.jpg\" alt=\"\" width=\"960\" height=\"601\" /></div>\r\n<p>Структурная штукатурка &ndash; неоднородная зернистая штукатурная масса имеющая добавку различных гранул, к примеру, древесных волокон, мелких камушков, кварцевых кусочков, слюды и другого. Изготавливается на минеральной основе, в основе лежит синтетический латекс или силикат калия. Штукатурки бывают на водной основе и на растворителях. Для внутренних помещений лучше использовать штукатурку на водной основе &ndash; без запаха, не нужна эвакуация из квартиры людей во время ремонта.</p>','products',1,NULL,1497360568,'f777d232898b6eeed0072c07536bc886.jpg'),(17,'Новости и статьи','Новости и статьи','Новости и статьи','Новости и статьи','Новости и статьи','','news',1,NULL,1497418534,'2b4086ffddb50792df1ef739447cae79.jpg'),(18,'Наши услуги','Наши услуги H1','Наши услуги title','keyword, s','Разнообразие возможностей в современном мире поражает – тенденции диктуют свои новые правила. Сегодня очень важно самовыражение, которое просвечивается сквозь призму привычек, желаний и вещей, окружающих каждого человека.','<div class=\"about__p\">Разнообразие возможностей в современном мире поражает &ndash; тенденции диктуют свои новые правила. Сегодня очень важно самовыражение, которое просвечивается сквозь призму привычек, желаний и вещей, окружающих каждого человека. Пространств, в котором мы проводим большую часть своего времени &ndash; это наш дом, нерушимое, безопасное пристанище, и именно поэтому оно должно соответствовать нашим требованием в уюте и комфорте изо дня в день. Пожалуй, отчасти мы сумели ответить на вопрос, почему так необходим дизайн квартир.</div>\r\n<p>&nbsp;</p>\r\n<ul class=\"about__head-list\">\r\n<li class=\"about__head-item about__head-item--type1\">\r\n<div class=\"about__head-block\">Максимально быстрое решение</div>\r\n</li>\r\n<li class=\"about__head-item about__head-item--type2\">\r\n<div class=\"about__head-block\">индивидуальный подход</div>\r\n</li>\r\n<li class=\"about__head-item about__head-item--type3\">\r\n<div class=\"about__head-block\">специальных условий для сотрудничества</div>\r\n</li>\r\n</ul>','services',1,NULL,1497429992,'b745c1ce3fb2117e83b105cb810bdbf5.jpg'),(23,'Портфолио','h1 seo portfolio','','','','','portfolio',1,NULL,1497361638,'449739edb6c45dab3e68de918815b8f7.jpg');
/*!40000 ALTER TABLE `control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron`
--

DROP TABLE IF EXISTS `cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cron`
--

LOCK TABLES `cron` WRITE;
/*!40000 ALTER TABLE `cron` DISABLE KEYS */;
/*!40000 ALTER TABLE `cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `alias` varchar(255) NOT NULL,
  `text` text,
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (8,1496752096,1496844489,1,'Реализованные объекты','4aa5672f6c148aa37e035ecc3de936b5.jpg','','','','','realizovannye-obekty','',2),(9,1496753970,1496844489,1,'ВИДЫ ФАКТУР','3f40125ace79a42a32c3a0378a7a0253.jpg','','','','','vidy-faktur','',1);
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_id` int(10) NOT NULL,
  `sort` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`) USING BTREE,
  CONSTRAINT `gallery_images_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_images`
--

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructions`
--

DROP TABLE IF EXISTS `instructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructions` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `module` varchar(75) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructions`
--

LOCK TABLES `instructions` WRITE;
/*!40000 ALTER TABLE `instructions` DISABLE KEYS */;
INSERT INTO `instructions` VALUES (1,'config','https://www.youtube.com/embed/ei0jwyvSwnI'),(2,'MailTemplates','https://www.youtube.com/embed/Ysiua0gHyK4'),(3,'log','https://www.youtube.com/embed/_BCCa7I8RS4'),(4,'callback','https://www.youtube.com/embed/KYVLmS8npXw'),(5,'blacklist','https://www.youtube.com/embed/8mRSL9kCfVU'),(6,'reviews','https://www.youtube.com/embed/dD_Xu8QokB8'),(7,'subscribe','https://www.youtube.com/embed/u97oKxJi-fs'),(8,'subscribers','https://www.youtube.com/embed/u97oKxJi-fs');
/*!40000 ALTER TABLE `instructions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_templates`
--

DROP TABLE IF EXISTS `mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_templates`
--

LOCK TABLES `mail_templates` WRITE;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
INSERT INTO `mail_templates` VALUES (28,'Контактная форма (Администратору)','Новое сообщение из контактной формы - сайт {{site}}','<p>Вам пришло новое письмо из контактной формы с сайта {{site}}!</p>\n<p>Имя отправителя: {{name}} ( {{ip}} ).</p>\n<p>Телефон отправителя: {{phone}}.</p>\n<p>IP отправителя: {{ip}}.</p>\n<p>Дата сообщения: {{date}}.</p>\n<p>Текст сообщения: {{text}}.</p>\n<p>&nbsp;</p>\n<p>Письмо сгенерировано автоматически. Пожалуйста не отвечайте на него!</p>',NULL,1,NULL);
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT '0',
  `name` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `id_content` int(11) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `image` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `count` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,0,'Панель управления','index/index',0,0,NULL,1,NULL,NULL,'fa-dashboard',NULL),(3,0,'Пользователи','users/index',10,0,NULL,1,NULL,NULL,'fa-users',NULL),(4,0,'Настройки сайта','config/edit',15,0,NULL,1,NULL,NULL,'fa-cogs',NULL),(5,0,'Контент',NULL,1,0,NULL,1,NULL,NULL,'fa-folder-open-o',NULL),(15,0,'Меню','menu/index',4,0,NULL,1,NULL,NULL,'fa-list-ul',NULL),(19,5,'Список новостей','news/index',4,0,NULL,1,NULL,NULL,NULL,NULL),(20,5,'Добавить новость','news/add',5,0,NULL,1,NULL,NULL,NULL,NULL),(25,64,'Список слайдов','slider/index',1,0,NULL,1,NULL,NULL,NULL,NULL),(26,64,'Добавть слайд','slider/add',2,0,NULL,1,NULL,NULL,NULL,NULL),(31,30,'Список подписчиков','subscribers/index',1,0,NULL,1,NULL,NULL,NULL,NULL),(32,30,'Добавить подписчика','subscribers/add',2,0,NULL,1,NULL,NULL,NULL,NULL),(33,30,'Список разосланных писем','subscribe/index',3,0,NULL,1,NULL,NULL,NULL,NULL),(34,30,'Разослать письмо','subscribe/send',4,0,NULL,1,NULL,NULL,NULL,NULL),(35,0,'Обратная связь',NULL,12,0,NULL,1,NULL,NULL,'fa-envelope-o','all_emails'),(36,35,'Сообщения из контактной формы','contacts/index',1,0,NULL,1,NULL,NULL,NULL,'contacts'),(59,5,'Системные страницы','control/index',3,0,NULL,1,NULL,NULL,NULL,NULL),(64,0,'Мультимедиа',NULL,6,0,NULL,1,NULL,NULL,'fa-picture-o',NULL),(66,3,'Список администраторов','admins/index',3,0,NULL,1,NULL,NULL,NULL,NULL),(67,3,'Добавить администратора','admins/add',4,0,NULL,1,NULL,NULL,NULL,NULL),(122,121,'Заблокированные адреса','blacklist/index',1,0,NULL,1,NULL,NULL,NULL,NULL),(123,121,'Заблокировать адрес','blacklist/add',2,0,NULL,1,NULL,NULL,NULL,NULL),(125,124,'Список отзывов','reviews/index',1,0,NULL,1,NULL,NULL,NULL,NULL),(126,124,'Добавить отзыв','reviews/add',2,0,NULL,1,NULL,NULL,NULL,NULL),(131,0,'Наши услуги',NULL,30,0,NULL,1,NULL,NULL,NULL,NULL),(134,131,'Список услуг','services/index',30,0,NULL,1,NULL,NULL,NULL,NULL),(135,131,'Добавить услугу','services/add',30,0,NULL,1,NULL,NULL,NULL,NULL),(138,0,'Портфолио','',35,0,0,1,NULL,NULL,NULL,NULL),(139,138,'Список','portfolio/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(140,138,'Добавить','portfolio/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(141,0,'Контакты','contact/index',39,0,NULL,1,NULL,NULL,NULL,NULL),(142,0,'Продукция','',100,0,NULL,1,NULL,NULL,NULL,NULL),(143,142,'Список','products/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(144,142,'Добавить','products/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(145,0,'SEO',NULL,35,0,NULL,1,NULL,NULL,'fa fa-tags',NULL),(146,145,'Шаблоны','seo_templates/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(147,145,'Теги для конкретных ссылок','seo_links/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(148,145,'Добавить теги для ссылок','seo_links/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(149,145,'Метрика и счётчики','seo_scripts/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(150,145,'Добавить метрику и счётчик','seo_scripts/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(151,145,'Перенаправления','seo_redirects/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(152,145,'Добавить перенаправление','seo_redirects/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(153,145,'Файлы','seo_files/index',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(154,145,'Добавить файл','seo_files/add',NULL,0,NULL,1,NULL,NULL,NULL,NULL),(155,0,'Шаблоны писем','mailTemplates/index',40,0,NULL,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `brand_alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`) USING BTREE,
  KEY `model_brand_alias` (`brand_alias`) USING BTREE,
  CONSTRAINT `models_ibfk_1` FOREIGN KEY (`brand_alias`) REFERENCES `brands` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  `show_image` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (29,1497332305,1497355914,1,'СОВРЕМЕННЫЕ АЛЬПИЙСКИЕ ИНТЕРЬЕРЫ  ЗА ТРАДИЦИОННЫМ ФАСАДОМ ВО ФРАНЦИИ','sovremennye-alpijskie-interery--za-traditsionnym-fasadom-vo-frantsii',1496437200,'<p>Это, внешне почти ничем не отличающееся от соседних коттеджей, шале внутри себя готовит своим гостям нечто большее. Помимо традиционной деревянной основы и больших окон с видом на окружающие горы, шале приятно удивляет своим минималистичным современным, но в тоже время мягким и теплым за счет подобранного текстиля и декора наполнением. В дизайне нашлось место очень интересным и даже забавным решениям: вязанные абажуры, мохнатые подушки и разделочные доски в качестве настенного декора. Здорово!</p>\r\n<p>Старый фермерский домик традиционной постройки в небольшом альпийском поселке во Франции приглянулся дизайнеру Sylvie Blanchet с первого взгляда. И самое главное его достоинство, которое &laquo;зацепило&raquo; будущих хозяев &mdash; шале хорошенько спрятано от посторонних глаз и дарит максимальное уединение, ведь находится вдалеке от популярных горнолыжных курортов с толпами туристов! После покупки Сильвия сразу приступила к реконструкции, чтобы превратить дом в современное уютного семейное гнездышко. Получилось просто здорово!</p>\r\n<p><img class=\"\" src=\"http://decor.project/Media/files/filemanager/elem_footer.jpg\" alt=\"\" width=\"859\" height=\"432\" /></p>','СОВРЕМЕННЫЕ АЛЬПИЙСКИЕ ИНТЕРЬЕРЫ  ЗА ТРАДИЦИОННЫМ ФАСАДОМ ВО ФРАНЦИИ H1','','','ключевые, слова','b6d10f11dae076cb2fd8e4cdf00c541d.jpg',0,21),(30,1497332812,1497437253,1,'ВОЗДУШНЫЙ ИНТЕРЬЕР КВАРТИРЫ В ГЕТЕБОРГЕ (70 КВ. М.)','vozdushnyj-interer-kvartiry-v-geteborge-70-kv-m',1497214800,'<p>Важнейшим достоинством камня как облицовочного материала можно считать его неограниченный срок службы.</p>','','','','','18f64d2e3c3812968bf5ee535b77b48e.jpg',0,5),(31,1497332877,1497437490,1,'СОВРЕМЕННЫЙ ВЗГЛЯД НА ТРАДИЦИОННЫЕ ','sovremennyj-vzgljad-na-traditsionnye',1497301200,'<p>Важнейшим достоинством камня как облицовочного материала можно считать его неограниченный срок службы.</p>','','','','','e5d7d5f3f986c845bb152d8dc1b08303.jpg',0,7),(32,1497332908,1497436995,1,'ИНТЕРЬЕР ГОСТИНОЙ В КОТТЕДЖЕ: 17 ИДЕЙ','interer-gostinoj-v-kottedzhe-17-idej',1497387600,'<p>Наличие коттеджа свидетельствует о состоятельности его владельца, о некоем статусе.</p>','','','','','',0,10),(33,1497351140,1497422746,1,'фывавыафыва','fyvavyafyva',1497214800,'<p>фываывафыва</p>','','','','',NULL,0,4);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `payment` int(2) DEFAULT NULL,
  `delivery` int(2) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `user_id` int(10) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `done` int(10) DEFAULT NULL COMMENT 'Дата когда заказ был выполнен',
  `changed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `popup_messages`
--

DROP TABLE IF EXISTS `popup_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `popup_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `ru` text,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `popup_messages`
--

LOCK TABLES `popup_messages` WRITE;
/*!40000 ALTER TABLE `popup_messages` DISABLE KEYS */;
INSERT INTO `popup_messages` VALUES (1,'message_after_oformleniya_basket','<p>Благодарим вас за заказ! <br /> Менеджер свяжется с вами в ближайшее время.</p>',2013),(2,'message_error_captcha','Вы неправильно ввели код безопасности.<br> Повторите, пожалуйста, отправку данных, внимательно указав код.',NULL),(3,'message_after_q_about_good','Ваш вопрос принят. Менеджер ответит вам в ближайшее время.',NULL),(4,'message_add_contact','Благодарим вас за сообщение!',NULL),(5,'message_add_comment_to_guestbook','Благодарим вас за оставленный отзыв. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.',NULL),(6,'message_add_comment_to_news','Благодарим вас за оставленный комментарий. <br>С вашей помощью наш сайт становится интереснее и полезнее. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.',NULL),(7,'message_error_login','Неправильно введен логин',NULL),(8,'message_after_registration','Благодарим вас за регистрацию на нашем сайте! Приятной работы!',NULL),(9,'message_text_after_registration_user_at_site','Благодарим вас за регистрацию на нашем сайте! <br /> На ваш email, указанный при регистрации, отправлено уведомление с данными для входа в ваш личный кабинет на сайте. <br /> Приятной работы!',NULL),(10,'message_after_autorisation','Данные введены правильно! Приятной работы!',NULL),(11,'message_text_after_autorisation_user_at_site','Добро пожаловать на наш сайт! <br /> Воспользуйтесь личным кабинетом для: редактирования своих данных и просмотра истории покупок. <br /> Приятной работы!',NULL),(12,'message_error_autorisation','Данные введены неправильно! Будьте внимательны!',NULL),(13,'message_after_exit','Возвращайтесь еще!',NULL),(14,'message_text_after_exit','Администрация сайта благодарит вас за время, потраченное на нашем сайте. До скорых встреч!',NULL),(16,'message_after_edit_data','Выши данные изменены. Приятной работы.',NULL),(17,'message_text_after_edit_data','Благодарим вас внимание к нашему сайту. <br /> На ваш email, указанный при регистрации, отправлено уведомление с измененными данными. <br /> Приятной работы!',NULL),(18,'subscribe_refuse','Вы отказались от рассылки на сайте!',NULL),(19,'subscribe_refuse_error','Вы не подписывались на рассылку на сайте!',NULL),(20,'subscribe_already01','уже является подписчиком на сайте!',NULL),(21,'subscribe_already02','введите другую почту для подписки.',NULL),(22,'subscribe_done','Вы подписались на рассылку на сайте',NULL);
/*!40000 ALTER TABLE `popup_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `alias` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `title` text CHARACTER SET cp1251,
  `description` text CHARACTER SET cp1251,
  `keywords` text CHARACTER SET cp1251,
  `text` text CHARACTER SET cp1251,
  `status` int(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `h1` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `class` varchar(64) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
INSERT INTO `portfolio` VALUES (4,'Реализованные проекты','realizovannye-proekty','','','','',1,1496906094,'',1496906148,0,0,NULL,0,'fadc444358faa3a1f83a83a46bbd96b5.jpg'),(5,'ВИДЫ ФАКТУР','vidy-faktur','','','','',1,1496906268,'',1496998708,0,0,NULL,0,'56a6b48f63c1d607a57515755073bcee.jpg'),(6,'облицовка стен квартиры','oblitsovka-sten-kvartiry','','Для проведения отделки фасада все чаще используется декоративная штукатурка короед. Нанести ее сможет даже подмастерье, главное, ознакомиться с технологией и максимально точно придерживаться ее.','','<p>Для проведения отделки фасада все чаще используется декоративная штукатурка короед. Нанести ее сможет даже подмастерье, главное, ознакомиться с технологией и максимально точно придерживаться ее. Для проведения отделки фасада все чаще используется декоративная штукатурка короед. Нанести ее сможет даже подмастерье, главное, ознакомиться с технологией и максимально точно придерживаться ее.</p>',1,1496906714,'',1496998349,0,4,NULL,0,'d3254dad6cc9a4e7e38398efdd5b5959.jpg');
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_images`
--

DROP TABLE IF EXISTS `portfolio_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `portfolio_id` int(10) NOT NULL,
  `sort` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `portfolio_id` (`portfolio_id`) USING BTREE,
  CONSTRAINT `portfolio_images_ibfk_1` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_images`
--

LOCK TABLES `portfolio_images` WRITE;
/*!40000 ALTER TABLE `portfolio_images` DISABLE KEYS */;
INSERT INTO `portfolio_images` VALUES (1,1496911277,NULL,'b4b9424ec99ced58459324361cc12bae.jpg',0,6,1),(2,1496911277,NULL,'c4f10f31541c173b9132cd22b67f42f5.jpg',0,6,2),(3,1496911277,NULL,'2461df6267329497ffc8ff6c2d78cfdd.jpg',0,6,3),(4,1496911277,NULL,'6ccce9047f4e37d312d1797c2d0b0925.jpg',0,6,4),(5,1496911278,NULL,'ed0c6b8282602a412df69ae6a93a43fd.jpg',0,6,5);
/*!40000 ALTER TABLE `portfolio_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  `show_image` tinyint(1) NOT NULL DEFAULT '1',
  `logo_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (6,1497257157,1497262583,1,'CAPAROL','<p>Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире. Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.</p>','','','','','73d2eff85f87600ade657312481d5275.jpg',0,'9982ed207276a481bd6316bb855e2510.jpg'),(7,1497263776,1497263797,1,'TEKNOS','<p>Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире. Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.</p>','','','','','e18ea513034738ad08926173adcd8339.jpg',0,'925b4fc25234a8c967ffccb54d3dc60b.jpg'),(8,1497263827,1497263839,1,'FERRARA PAINT','<p>Безупречное качество материалов Caparol обеспечивается множеством инновационных процессов, обеспечиваемых высокотехнологичным оборудованием и трудом высококвалифицированных сотрудников по всему миру. Продукция бренда Caparol давно приобрела статус высококачественной в лице своих покупателей во всем мире.</p>','','','','',NULL,0,'d64da01dd5cb386676c89710de7d5e10.jpg');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_links`
--

DROP TABLE IF EXISTS `seo_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_links` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `link` (`link`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_links`
--

LOCK TABLES `seo_links` WRITE;
/*!40000 ALTER TABLE `seo_links` DISABLE KEYS */;
INSERT INTO `seo_links` VALUES (1,'name',1,1497354297,NULL,'/news','{{name}}','{{name}}','{{description}}','{{name}}','');
/*!40000 ALTER TABLE `seo_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_redirects`
--

DROP TABLE IF EXISTS `seo_redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_redirects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `link_from` varchar(255) DEFAULT NULL,
  `link_to` varchar(255) DEFAULT NULL,
  `type` int(4) NOT NULL DEFAULT '300',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_redirects`
--

LOCK TABLES `seo_redirects` WRITE;
/*!40000 ALTER TABLE `seo_redirects` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_redirects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_scripts`
--

DROP TABLE IF EXISTS `seo_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_scripts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `script` text,
  `status` int(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `place` varchar(31) DEFAULT 'head',
  PRIMARY KEY (`id`),
  KEY `place` (`place`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_scripts`
--

LOCK TABLES `seo_scripts` WRITE;
/*!40000 ALTER TABLE `seo_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_templates`
--

DROP TABLE IF EXISTS `seo_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `h1` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_templates`
--

LOCK TABLES `seo_templates` WRITE;
/*!40000 ALTER TABLE `seo_templates` DISABLE KEYS */;
INSERT INTO `seo_templates` VALUES (4,'Шаблон для новости','{{h1}} ','{{title}}','{{description}}','{{keywords}}',1497355712),(5,'Шаблон для портфолио','{{h1}}','{{title}}','{{description}}','{{keywords}}',1497356051);
/*!40000 ALTER TABLE `seo_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (5,1,'ПРОДАЖА ЛАКОКРАСОЧНЫХ МАТЕРИАЛОВ','',NULL,'<p>Лакокрасочные материалы являются важнейшим строительным материалом, от которого зависит то, насколько обновленным и посвежевшим будет выглядеть интерьер вашего дома после ремонта. Рынок лакокрасочных материалов, представлен достаточно широко и постоянно развивается, внедряя новые технологии и компоненты для ЛКМ.</p>',NULL,NULL,NULL,NULL,'4fae0555a68ba0bd9fdcf7fa0c59fdd7.jpg'),(6,1,'ВЫПОЛНЕНИЕ МАЛЯРНЫХ РАБОТ','',NULL,'<p>Лакокрасочные материалы являются важнейшим строительным материалом, от которого зависит то, насколько обновленным и посвежевшим будет выглядеть интерьер вашего дома после ремонта. Рынок лакокрасочных материалов, представлен достаточно широко и постоянно развивается, внедряя новые технологии и компоненты для ЛКМ.</p>',NULL,NULL,NULL,NULL,'bdfff26a69e2d93be116407968327abf.jpg');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `tpl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemap`
--

LOCK TABLES `sitemap` WRITE;
/*!40000 ALTER TABLE `sitemap` DISABLE KEYS */;
INSERT INTO `sitemap` VALUES (1,'content','Контентовые страницы',0,1,NULL,2,'content'),(2,'contact','Контакты',0,1,NULL,3,NULL),(3,'news','Новости',0,1,NULL,4,'news'),(4,'news_list','Список новостей',3,1,NULL,1,NULL),(5,'articles','Статьи',0,1,NULL,5,'articles'),(6,'articles_list','Список статей',5,1,NULL,1,NULL),(7,'blog','Блог',0,1,NULL,6,'blog'),(8,'blog_rubrics','Список рубрик',7,1,NULL,1,NULL),(9,'blog_list','Список статей',7,1,NULL,2,NULL),(10,'gallery','Фотогаллерея',0,1,NULL,7,'gallery'),(11,'gallery_list','Список альбомов',10,1,NULL,1,NULL),(12,'reviews','Отзывы',0,1,NULL,8,NULL),(13,'catalog','Каталог',0,1,NULL,9,'catalog'),(14,'catalog_groups','Список групп',13,1,NULL,1,NULL),(15,'catalog_items','Список товаров',14,1,NULL,1,NULL),(16,'catalog_sale','Акции',13,1,NULL,3,'sale'),(17,'catalog_new','Новинки',13,1,NULL,4,'new'),(18,'catalog_popular','Популярные',13,1,NULL,5,'popular'),(19,'catalog_brands','Производители',13,1,NULL,6,'brands'),(21,'sitemap','Карта сайта',0,1,NULL,10,NULL),(22,'index','Главная страница',0,1,NULL,1,NULL),(23,'brands_list','Список производителей',19,1,NULL,1,NULL),(24,'products','Продукты',NULL,1,NULL,10,NULL);
/*!40000 ALTER TABLE `sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemenu`
--

DROP TABLE IF EXISTS `sitemenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemenu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemenu`
--

LOCK TABLES `sitemenu` WRITE;
/*!40000 ALTER TABLE `sitemenu` DISABLE KEYS */;
INSERT INTO `sitemenu` VALUES (12,1496738353,1496998147,1,0,'Главная','/',NULL),(13,1496738370,1496998147,1,4,'Продукция','/products',NULL),(14,1496738512,1497244274,1,5,'Наши услуги','/services',0),(15,1496738527,1496998147,1,6,'Новости и статьи','news',NULL),(16,1496738544,1497244259,1,7,'Портфолио','/portfolio',0),(17,1496738554,1496998147,1,9,'Контакты','contact',NULL),(19,1496988699,1497003129,1,8,'ВИДЫ ФАКТУР','/vidy-faktur',16),(20,1496989330,1497003112,1,1,'Реализованные проекты','/realizovannye-proekty',16),(21,1496991672,1497003303,1,3,'Частные дома','/chastnie-doma',20),(22,1496997904,1497003293,1,2,'Квартиры','/kvartiry',20),(23,1497003498,1497003506,1,0,'Облицовка стен квартиры','/oblitsovka-sten-kvartiry',20);
/*!40000 ALTER TABLE `sitemenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (6,1496744758,NULL,1,0,'','','75491ba67d5ba829b3228265ef1e0a45.jpg',''),(7,1496745643,1496745811,1,0,'Уникальная штукатурка','','53b970b742f9f1a994aab267ae19b650.jpg','Сучасний дизайн інтер\'єру - неперевершене мистецтво створення затишної і неповторної домашньої атмос');
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specifications`
--

DROP TABLE IF EXISTS `specifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `type_id` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  KEY `type_id` (`type_id`) USING BTREE,
  CONSTRAINT `specifications_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `specifications_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specifications`
--

LOCK TABLES `specifications` WRITE;
/*!40000 ALTER TABLE `specifications` DISABLE KEYS */;
INSERT INTO `specifications` VALUES (19,1447093027,1464684912,1,'Материал',6,'material',3),(20,1447093044,NULL,1,'Производство',5,'proizvodstvo',2),(21,1447093054,NULL,1,'Сезон',3,'sezon',3),(22,1447093064,1447095073,1,'Цвет',2,'tsvet',1),(23,1447093519,NULL,1,'Половой признак',4,'sex',2),(24,1463997483,1464196645,1,'Описание',1,'opisanie',2),(25,1465039912,1483518854,1,'новинка',0,'novinka',1);
/*!40000 ALTER TABLE `specifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specifications_types`
--

DROP TABLE IF EXISTS `specifications_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specifications_types` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specifications_types`
--

LOCK TABLES `specifications_types` WRITE;
/*!40000 ALTER TABLE `specifications_types` DISABLE KEYS */;
INSERT INTO `specifications_types` VALUES (1,'Цвет'),(2,'Обычная'),(3,'Мультивыбор');
/*!40000 ALTER TABLE `specifications_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specifications_values`
--

DROP TABLE IF EXISTS `specifications_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specifications_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `specification_id` int(10) NOT NULL,
  `color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  KEY `specification_id` (`specification_id`) USING BTREE,
  CONSTRAINT `specifications_values_ibfk_1` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specifications_values`
--

LOCK TABLES `specifications_values` WRITE;
/*!40000 ALTER TABLE `specifications_values` DISABLE KEYS */;
INSERT INTO `specifications_values` VALUES (61,1447093079,NULL,1,'Замша',0,'zamsha',19,NULL),(62,1447093084,NULL,1,'Искусственная замша',0,'iskusstvennajazamsha',19,NULL),(63,1447093089,NULL,1,'Искусственная кожа',0,'iskusstvennajakozha',19,NULL),(64,1447093092,NULL,1,'Кожа',0,'kozha',19,NULL),(65,1447093097,NULL,1,'Латекс',0,'lateks',19,NULL),(66,1447093101,NULL,1,'Нейлон',0,'nejlon',19,NULL),(67,1447093107,NULL,1,'Нубук',0,'nubuk',19,NULL),(68,1447093112,NULL,1,'Полиестр',0,'poliestr',19,NULL),(69,1447093116,NULL,1,'Полимер',0,'polimer',19,NULL),(70,1447093120,NULL,1,'Силикон',0,'silikon',19,NULL),(71,1447093125,NULL,1,'Синтетика',0,'sintetika',19,NULL),(72,1447093130,NULL,1,'Текстиль',0,'tekstil',19,NULL),(73,1447093142,NULL,1,'Хлопок',0,'hlopok',19,NULL),(74,1447093164,NULL,1,'Китай',0,'kitaj',20,NULL),(75,1447093169,NULL,1,'Украина',0,'ukraina',20,NULL),(76,1447093177,NULL,1,'США',0,'ssha',20,NULL),(77,1447093182,NULL,1,'Италия',0,'italija',20,NULL),(78,1447093190,NULL,1,'Вьетнам',0,'vetnam',20,NULL),(79,1447093196,NULL,1,'Индонезия',0,'indonezija',20,NULL),(80,1447093205,NULL,1,'Польша',0,'polsha',20,NULL),(81,1447093215,NULL,1,'Лето',1,'leto',21,NULL),(82,1447093220,NULL,1,'Зима',0,'zima',21,NULL),(83,1447093225,NULL,1,'Весна',2,'vesna',21,NULL),(84,1447093230,NULL,1,'Осень',3,'osen',21,NULL),(85,1447093257,NULL,1,'Бежевый',1,'beige',22,'#f0daad'),(86,1447093270,NULL,1,'Белый',0,'white',22,'#ffffff'),(87,1447093288,NULL,1,'Бирюзовый',2,'turquoise',22,'#4ff58c'),(88,1447093303,NULL,1,'Бордовый',3,'claret',22,'#7a0b21'),(89,1447093316,NULL,1,'Голубой',4,'light_blue',22,'#00bfff'),(90,1447093343,NULL,1,'Желтый',5,'yellow',22,'#f2ff00'),(91,1447093355,NULL,1,'Зеленый',6,'green',22,'#0dbd00'),(92,1447093370,NULL,1,'Коралловый',7,'coral',22,'#ff6666'),(93,1447093388,NULL,1,'Коричневый',8,'korichnevyj',22,'#802400'),(94,1447093397,NULL,1,'Красный',9,'krasnyj',22,'#ff0000'),(95,1447093413,NULL,1,'Розовый',10,'rozovyj',22,'#ff0077'),(96,1447093429,NULL,1,'Серый',11,'seryj',22,'#8c8c8c'),(97,1447093438,NULL,1,'Синий',12,'sinij',22,'#4000ff'),(98,1447093453,NULL,1,'Фиолетовый',13,'fioletovyj',22,'#9900ff'),(99,1447093461,NULL,1,'Черный',14,'chernyj',22,'#000000'),(100,1447093527,NULL,1,'Мужской',0,'muzhskoj',23,NULL),(101,1447093531,NULL,1,'Женский',0,'zhenskij',23,NULL),(102,1447093536,NULL,1,'Унисекс',0,'uniseks',23,NULL);
/*!40000 ALTER TABLE `specifications_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe_mails`
--

DROP TABLE IF EXISTS `subscribe_mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe_mails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` mediumtext,
  `emails` longtext,
  `count_emails` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe_mails`
--

LOCK TABLES `subscribe_mails` WRITE;
/*!40000 ALTER TABLE `subscribe_mails` DISABLE KEYS */;
INSERT INTO `subscribe_mails` VALUES (1,1464178072,NULL,'лоижлио','<p>жлдоижилдо</p>','veselovskaya.wezom@gmail.com',1);
/*!40000 ALTER TABLE `subscribe_mails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET cp1251 DEFAULT NULL,
  `login` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `password` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `hash` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(2) NOT NULL DEFAULT '1',
  `ip` varchar(16) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `last_login` int(10) DEFAULT NULL,
  `logins` int(10) DEFAULT '0',
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`) USING BTREE,
  UNIQUE KEY `hash` (`hash`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Администратор','admin','c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8',1418300546,1430939378,'48e00180ccc77b94d73ec413b015a4cfb9aa58ba10d8c1b63aad5c8317847d9f','palenaya.v.wezom111@gmail.com',1,4,NULL,'+38 (111) 111-11-11',NULL,0,NULL,NULL),(2,'weZom','wezom','4958070fab7cebd8b1000c6c8cb1bca4aa23b509ee9bc4b70570b5c0e3dfe64a',NULL,1435164507,'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8','vitaliy.demyjkane3nko.1991@gmail.com',1,3,NULL,'+38 (567) 567-56-75',NULL,0,NULL,NULL),(21,'Виталя',NULL,'d7957677dc4ec0e9e425c8b99e477aa70244ae512fc2583af6a1ab2ee3b01fbc',1447096765,1448350164,'d6fb647d44ada79149c6732227667ea44b84ab78706cffe94027f960d0bb1124','demyanenko.v.wezom@ukr.net',1,1,'127.0.0.1','',1448350164,3,'Демяненко','444'),(22,'Test1','admin1','c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8',1447426756,1447426756,NULL,NULL,1,5,NULL,NULL,NULL,0,NULL,NULL),(23,'Anton',NULL,'3e61c90127ae20abbcc8b1b7cfabf301a0269de0460890d5ec33373870c4e172',1447515858,NULL,'409fe348ba859b01e942532e108ff6479dd435feaae09d81adc9aa91871a0de0','crocoash@gmail.com',1,1,'109.86.230.160',NULL,1447515857,1,'Tsvetkov',NULL),(24,'Vitaliy',NULL,'339e548c63121ea40d9b8d9a7e6bc43279ba802991a1d2dd54ec8c080328b826',1448910941,NULL,'3d63d55d0f1a64f2ad0bd1255b4ecbdbfc0820362d2f1456b64556439fc1e5a8','demyanenko.v.wezom@gmail.com',1,1,'46.175.163.12',NULL,1448910941,1,'Demianenko',NULL),(25,'zorya','zorya','131cf002c1c268432c00b026a5182ada3c14a3cbd3ec2f112ea2aff21f4e621e',1450795155,1483520734,NULL,NULL,1,7,NULL,NULL,NULL,0,NULL,NULL),(26,'test',NULL,'99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f',1461673130,NULL,'73ffbeef661a2707b9bea04b39979a2a99655a8ae4eed79a35869f20bdec40a3','test.wezom@mail.ru',0,1,NULL,'',NULL,0,'',''),(27,'Екатерина',NULL,'ce7500fb2f28f5d742c9821a1bd7847e8375065070d0e137c401cd151f8b7564',1464541221,NULL,'d9201dfdc8a7585db6d597b570cbaddf8e8288408e5713f887baff81ec9daadf','katia-katerinka_@mail.ru',1,1,'83.142.111.151',NULL,1464541221,1,'Баськова',NULL),(28,'test',NULL,'99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f',1465467766,1483520720,'398518fd32869473b46bb3f54c02cb85e86421138514e01b9e85a23b79edc9e9','test1@mail.ru',0,1,NULL,'',NULL,0,'','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_networks`
--

DROP TABLE IF EXISTS `users_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_networks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `network` varchar(32) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  `profile` varchar(128) NOT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `users_networks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_networks`
--

LOCK TABLES `users_networks` WRITE;
/*!40000 ALTER TABLE `users_networks` DISABLE KEYS */;
INSERT INTO `users_networks` VALUES (7,21,'vkontakte',1447096768,'22898418','http://vk.com/id22898418','Виталя','Демяненко','demyanenko.v.wezom@ukr.net'),(8,21,'facebook',1447096775,'100003261046861','http://www.facebook.com/100003261046861','Vitaliy','Demianenko','vitaliy.demyanenko.1991@gmail.com'),(9,23,'facebook',1447515858,'1628676114064038','https://www.facebook.com/app_scoped_user_id/1628676114064038/','Anton','Tsvetkov','crocoash@gmail.com'),(10,24,'instagram',1448910941,'1685676043','http://instagram.com/demianenko_v','Vitaliy','Demianenko','demyanenko.v.wezom@gmail.com'),(11,27,'vkontakte',1464541222,'3646497','http://vk.com/id3646497','Екатерина','Баськова','katia-katerinka_@mail.ru');
/*!40000 ALTER TABLE `users_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,'Пользователь','Обычный пользователь, зарегистрировавшийся на сайте','user',NULL,NULL),(3,'Разработчик','Полный доступ ко всему + к тому к чему нет доступа у главного администратора','developer',NULL,NULL),(4,'Суперадмин','Доступ ко всем разделам, кроме тех, к которым имеет доступ только разработчик','superadmin',NULL,NULL),(5,'1','1','admin',1447421386,NULL),(6,'rol1','запрещено все','admin',1447662341,NULL),(7,'все разрешено','','admin',1460728748,1483520750);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-15 11:41:34
