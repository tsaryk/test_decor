var inkubatorPreviewerData = {
	"list": [
		{
			"alias": "index",
			"title": "Главная"
		},
		{
			"alias": "news",
			"title": "Новости и статьи"
		},
		{
			"alias": "products",
			"title": "Продукция"
		},
		{
			"alias": "portfolio",
			"title": "Портфолио"
		},
		{
			"alias": "project",
			"title": "Реализованные проекты"
		},
		{
			"alias": "gallery",
			"title": "Дизайн интерьера квартиры..."
		},
		{
			"alias": "news_item",
			"title": "Современные альпийские интерьеры..."
		},
		{
			"alias": "contact",
			"title": "Контакты"
		},
		{
			"alias": "about",
			"title": "наши услуги"
		},
		{
			"alias": "404",
			"title": "404"
		},
		{
			"alias": "typography",
			"title": "Типография сайта"
		}
	],
	"home": "index",
	"side": "left"
};