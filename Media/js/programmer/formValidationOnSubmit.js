// ============================================= //
//		Это демо файл							 //
//		js код нужно перенсти					 //
//		в файл скриптовых инициализаций php		 //
// ============================================= //
var generate = function( message, type, time ) {
    var mainBlock = $('#fPopUp');
    var current;
    if(!mainBlock.length) {
        $('<div id="fPopUp"></div>').appendTo('body');
        mainBlock = $('#fPopUp');
    }
    var i = 1;
    var count = 0;
    mainBlock.find('.content').each(function(){
        current = parseInt($(this).data('i'));
        if(current + 1 > i) {
            i = current + 1;
        }
        count++;
    });
    if(count >= 5) {
        mainBlock.find('div.content:first-child').remove();
    }
    $('<div class="content ' + type + '" data-i="' + i + '" style="display: none;">' + message + '</div>').appendTo(mainBlock);
    mainBlock.find('div.content[data-i="' + i + '"]').fadeIn(200);
    if(time) {
        setTimeout(function(){
            closePopup(mainBlock.find('div.content[data-i="' + i + '"]'));
        }, time);
    }
    window.location.reload();
};

var closePopup = function( it ) {
    it.fadeOut(650);
    setTimeout(function(){
        it.remove();
    }, 700);
};

$('form#contactForm').submit(function () {
    wHTML.formOnSubmit($('form#contactForm'));

    (function($){

        wHTML.formOnSubmit = function (formJQ) {
            if (formJQ.data('ajax')) {
                var data = new FormData($('form#contactForm'));
                var name;
                var val;
                var type;
                formJQ.find('input,textarea,select').each(function () {
                    var thisJQ = $(this);
                    name = thisJQ.data('name');
                    val = this.value;
                    type = this.type;
                    if ((type != 'checkbox' && name) || (type == 'checkbox' && this.checked && name)) {
                        if (type == 'file') {
                            data.append(name, $(this)[0].files[0]);
                        } else if (type == 'radio' && $(this).prop('checked')) {
                            data.append(name, val);
                        } else if (type != 'radio') {
                            data.append(name, val);
                        }
                    }
                });
                var request = new XMLHttpRequest();
                request.open("POST", '/form/' + formJQ.data('ajax'));
                request.onreadystatechange = function () {
                    var status;
                    var resp;
                    if (request.readyState == 4) {
                        status = request.status;
                        resp = request.response;
                        alert(resp);
                        console.log(resp);
                        resp = jQuery.parseJSON(resp);
                        if (status == 200) {
                            if (resp.success) {
                                if (resp.clear) {
                                    formJQ.find('input').each(function () {
                                        if ($(this).attr('type') != 'hidden' && $(this).attr('type') != 'checkbox') {
                                            $(this).val('');
                                        }
                                    });
                                    formJQ.find('textarea').val('');
                                }
                                if (resp.insert && resp.insert.selector && resp.insert.html) {
                                    $(resp.insert.selector).html(resp.insert.html);
                                }
                                if (resp.response) {
                                    generate(resp.response, 'success', 3500);
                                    window.location.reload();
                                }
                            } else {
                                if (resp.response) {
                                    generate(resp.response, 'warning', 3500);
                                }
                            }
                            if (resp.redirect) {
                                if (window.location.href == resp.redirect) {
                                    window.location.reload();
                                } else {
                                    window.location.href = resp.redirect;
                                }
                            }
                            if (resp.reload) {
                                window.location.reload();
                            }
                            wHTML.formValidationAfterSubmit( $form, resp );
                        } else {
                            alert('Something went wrong,\nbut HTML fine ;)');
                        }
                    }

                };
                request.send(data);
                window.location.reload();
                return false;
            }

        };
    })(jQuery);

    $(function($) {
		wHTML.formValidationAfterSubmit = function ($form, resp) {
			$form.attr('style', 'display: none;');
        }
    })(jQuery);
});

