<div class="hidden-wrapper hidden-wrapper--md">
	<div class="contact__form">
		<h2 class="title title--contact">обратная связь</h2>
		<div class="form js-form" data-form="true">
			<div class="form__group">
				<div class="form__input control-holder control-holder--text">
					<input required type="text" id="name" name="name" data-rule-minlength="2" data-rule-word="true" placeholder="ваше имя">
				</div>
				<div class="form__input control-holder control-holder--text">
					<input class="js-inputmask" required type="tel" id="tel" name="tel" placeholder="+38(___)___-__-__" data-mask="+38(099)9999999">
				</div>
				<div class="form__input control-holder control-holder--text">
					<input required type="text" id="message" name="message" data-rule-minlength="10" placeholder="сообщение">
				</div>
			</div>
			<button class="but js-form-submit">
				<span>Отправка формы</span>
			</button>
		</div>
	</div>
</div>
