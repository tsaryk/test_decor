<?php
namespace Core;

use Modules\News\Models\News;
use Core\QB\DB;

/**
 *  Class that helps with widgets on the site
 */
class Widgets
{

    static $_instance; // Constant that consists self class

    public $_data = []; // Array of called widgets
    public $_tree = []; // Only for catalog menus on footer and header. Minus one query

    // Instance method
    static function factory()
    {
        if (self::$_instance == NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *  Get widget
     * @param  string $name [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string        [Widget HTML]
     */
    public static function get($name, $array = [], $save = true, $cache = false)
    {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

        if (APPLICATION == 'backend' && !Config::get('error')) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if ($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else if (method_exists($w, $name)) {
                        $result = $w->$name($array);
                        if ($result !== null && $result !== false) {
                            $array = array_merge($array, $result);
                            $data = View::widget($array, $viewpath);
                        } else {
                            $data = null;
                        }
                    } else {
                        $data = $w->common($viewpath, $array);
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if ($_cache->get($name)) {
            $_cache->delete($name);
        }
        if ($save && isset($w->_data[$name])) {
            return $w->_data[$name];
        }
        if (method_exists($w, $name)) {
            $result = $w->$name($array);
            if ($result !== null && $result !== false) {
                if (is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget($array, $viewpath);
            } else {
                return $w->_data[$name] = null;
            }
        }
        return $w->_data[$name] = $w->common($viewpath, $array);
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     * @param  string $viewpath [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common($viewpath, $array)
    {
        if (file_exists(HOST . '/Views/Widgets/' . $viewpath . '.php')) {
            return View::widget($array, $viewpath);
        }
        return null;
    }

    public function HiddenData()
    {
        $cart = Cart::factory()->get_list_for_basket();
        return ['cart' => $cart];
    }

    public function Index_Slider()
    {
        $result = Common::factory('slider')->getRows(1, 'sort');
        if (!sizeof($result)) {
            return false;
        }
        return ['result' => $result];
    }

    public function Footer()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
        $array['contentMenu'] = $contentMenu;
        // phones, adress, email
        $array['footerData'] = [
            'copyright' => Config::get('static.copyright'),
            'firstPhone' => Config::get('static.phone'),
            'secondPhone' => Config::get('static.second_phone'),
            'thrirdPhone' => Config::get('static.thrird_phone'),
            'email' => Config::get('static.footer_email'),
            'address' => Config::get('static.footer_address'),
            'fb' => Config::get('socials.fb'),
            'vk' => Config::get('socials.vk'),
            'instagram' => Config::get('socials.instagram'),
        ];
        $array['scripts'] = [
//            HTML::media('js/jquery-1.11.0.min.js', false),
            HTML::media('js/modernizr.js', false),
            HTML::media('js/basket.js', false),
            HTML::media('js/plugins.js', false),
            HTML::media('js/init.js', false),
            HTML::media('js/wPreloader.js', false),
            HTML::media('js/programmer/my.js', false),
            HTML::media('js/programmer/_my.js', false),
        ];
        $array['scripts_no_minify'] = [
            HTML::media('js/programmer/ulogin.js', false),
        ];
        return $array;
    }

    public function Header()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort')->as_array();

        $arr = [];
        foreach($contentMenu AS $obj) {
            $idParent = empty($obj->id_parent) ? 0 : $obj->id_parent;

            $arr[$idParent][] = $obj;
        }

        $array['contentMenu'] = $arr;
        return $array;
    }

    public function Head()
    {
        $styles = [
            HTML::media('css/plugin.css', false),
            HTML::media('css/style.css', false),
//                HTML::media('css/programmer/magnific.css'),
            HTML::media('css/programmer/fpopup.css', false),
            HTML::media('css/programmer/my.css', false),
            HTML::media('css/responsive.css', false),
            HTML::media('css/wPreloader.css', false),
        ];

        return ['styles' => $styles];
    }

    public function GetBreadCrumbs($_breadcrumbs)
    {
        return $_breadcrumbs;
    }

    public function Index_OurWorks()
    {
        $portfolio = Common::factory('gallery')->getRows(1)->as_array();

        return ['portfolio' => $portfolio];
    }

    public function Index_OurServices()
    {
        $services = Common::factory('services')->getRows(1)->as_array();
        $service_page = Common::factory('control')->getRow('services', 'alias');

        return ['services' => $services, 'systemPage' => $service_page];
    }

    public function Index_OurAchievements()
    {
        $config = [
            'left_top' => Config::get('achievements.left_top'),
            'left_center' => Config::get('achievements.left_center'),
            'left_down' => Config::get('achievements.left_down'),
            'center_top' => Config::get('achievements.center_top'),
            'center_block' => Config::get('achievements.center_block'),
            'center_down' => Config::get('achievements.center_down'),
            'right_top' => Config::get('achievements.right_top'),
            'right_block' => Config::get('achievements.right_center'),
            'right_down' => Config::get('achievements.right_down'),
        ];

        return ['config' => $config];
    }

    public function Index_LastNews()
    {
        $news = News::getRows(1, 'date', 'DESC', 3)->as_array();

        return ['result' => $news];
    }

    public function Index_TextSection()
    {
        $controlPage = Common::factory('control')->getRow('index', 'alias');

        return ['result' => $controlPage];
    }

    public function News_LastNews($data)
    {
        $content = News::getRowsWithoutID(1, null, null, 3, null, true, $data['currentItemID'])->as_array();

        return ['result' => $content];
    }

    public function News_NextAndPrevItems($data)
    {
        $items = News::getRowsWithoutID(1, null, nul, 2, null, true, $data['currentItemID'])->as_array();

        $returnData = [
            'nextItem' => count($items) == 2 ? current($items) : null,
            'prevItem' => isset($items) && !empty($items) ? current($items) : null
        ];

        return $returnData;
    }

    public function FirstSection($data = array())
    {
        return $data;
    }
}