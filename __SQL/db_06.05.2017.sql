# add new static fields
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Номер телефона в шапке сайта', '0951234567', 1, 'thrird_phone', 0, 'input', 'static', 8);
insert into config(name, zna, status, `key`, valid, type, `group`)
values('Номер телефона в футере сайта', '0951234567', 1, 'thrird_phone', 0, 'input', 'static', 9);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('E-mail в подвале сайта', 'tikkurila.kherson@gmail.com', 1, 'footer_email', 0, 'input', 'static', 10);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Адрес в подвале сайта', 'ХЕРСОН, УЛ. СТРЕТЕНСКАЯ, 11-Б', 1, 'footer_address', 0, 'input', 'static', 11);
# add new field to slider
alter table slider
add column text varchar(255) null
after image;
# add items to admin menu
insert into menu(name, sort) values('Наши услуги', 30);
insert into menu(id_parent, name, link) values(131, 'Список услуг', 'services/index');
insert into menu(id_parent, name, link) values(131, 'Добавить услугу', 'services/add');
# create table services
create table services(
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
# create new tab in settings page
insert into config_groups values(null, 'Блок достижений', 'achievements', 'left', 1, 6);
# add new fields for achievement block
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Левый блок, текст сверху', 'более', 1, 'left_top', 0, 'input', 'achievements', 1);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Левый блок, количетсво', '200', 1, 'left_center', 0, 'input', 'achievements', 2);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Левый блок, текст снизу', '200', 1, 'left_down', 0, 'input', 'achievements', 3);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Блок по-центру, текст сверху', 'более', 1, 'center_top', 0, 'input', 'achievements', 4);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Блок по-центру, количетсво', '200', 1, 'center_block', 0, 'input', 'achievements', 5);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Блок по-центру, текст снизу', '200', 1, 'center_down', 0, 'input', 'achievements', 6);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Правый блок, текст сверху', 'более', 1, 'right_top', 0, 'input', 'achievements', 7);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Правый блок, количетсво', '200', 1, 'right_block', 0, 'input', 'achievements', 8);
insert into config(name, zna, status, `key`, valid, type, `group`, sort)
values('Правый блок, текст снизу', '200', 1, 'right_down', 0, 'input', 'achievements', 9);
# create products system page
insert into control(name, alias, status) values('Продукция', 'products', 1);
# add gallery(portfolio) system page
insert into control(name, alias, status) values('Портфолио', 'portfolio', 1);
insert into menu (name, link, id_parent) values ('Список', 'portfolio/index', 138);
insert into menu (name, link, id_parent) values ('Добавить', 'portfolio/add', 138);
# add new table 'portfolio'
DROP TABLE IF EXISTS `portfolio`;
CREATE TABLE `portfolio` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `alias` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `title` text CHARACTER SET cp1251,
  `description` text CHARACTER SET cp1251,
  `keywords` text CHARACTER SET cp1251,
  `text` text CHARACTER SET cp1251,
  `status` int(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `h1` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `class` varchar(64) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) default null,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`alias`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
# add table 'portfolio images'
DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE `portfolio_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `portfolio_id` int(10) NOT NULL,
  `sort` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `portfolio_id` (`portfolio_id`) USING BTREE,
  CONSTRAINT `portfolio_images_ibfk_1` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
# add new item to 'control pages'
insert into control(name, h1, title, keywords, description, alias, status)
values('Новости и статьи', 'Новости и статьи', 'Новости и статьи', 'Новости и статьи', 'Новости и статьи', 'news', 1);
# add new table for contact page
create table contact_page(
  map text null,
  status tinyint(1) default 1
);
# add new page to admin area
insert into menu(name, link, status) values('Контакты', 'contact_page/index', 1);
insert into contact_page(status) value(0);
# add new system page - 'services'
insert into control(name, alias, status) values('Наши услуги', 'services', 1);
# remove system pages
delete from control where alias = 'sitemap';
delete from control where alias = 'about';
delete from control where alias = 'search';
delete from control where alias = 'catalog';
# remove other modules
DELETE FROM `decor_db`.`menu` WHERE `id`='115';
DELETE FROM `decor_db`.`menu` WHERE `id`='116';
DELETE FROM `decor_db`.`menu` WHERE `id`='117';
DELETE FROM `decor_db`.`menu` WHERE `id`='118';
DELETE FROM `decor_db`.`menu` WHERE `id`='119';
DELETE FROM `decor_db`.`menu` WHERE `id`='120';
# remove module 'orders'
DELETE FROM `decor_db`.`menu` WHERE `id`='50';
DELETE FROM `decor_db`.`menu` WHERE `id`='51';
DELETE FROM `decor_db`.`menu` WHERE `id`='52';
# remove module 'mail templates'
DELETE FROM `decor_db`.`menu` WHERE `id`='14';
# remove parts of moduke "catalog"
DELETE FROM `decor_db`.`menu` WHERE `id`='39';
DELETE FROM `decor_db`.`menu` WHERE `id`='40';
DELETE FROM `decor_db`.`menu` WHERE `id`='43';
DELETE FROM `decor_db`.`menu` WHERE `id`='44';
DELETE FROM `decor_db`.`menu` WHERE `id`='45';
DELETE FROM `decor_db`.`menu` WHERE `id`='46';
DELETE FROM `decor_db`.`menu` WHERE `id`='47';
DELETE FROM `decor_db`.`menu` WHERE `id`='48';
DELETE FROM `decor_db`.`menu` WHERE `id`='49';
DELETE FROM `decor_db`.`menu` WHERE `id`='53';
# add new static field
insert into config(name, zna, status,  sort, `key`, type, `group`)
values('Карта на странице контакты', 'test', 1, 20, 'contact_map', 'textarea', 'static');
# create table products
-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  `show_image` tinyint(1) NOT NULL DEFAULT '1',
  'logo_image' varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
# add item to menu
insert into menu(name, link, status, sort)
values('Продукция', '', 1, 100);
insert into menu(name, link, status, id_parent)
values('Список', 'products/index', 1, 142);
insert into menu(name, link, status, id_parent)
values('Добавить', 'products/add', 1, 142);
