<?php

return [
    [
        'name' => 'Панель управления',
        'module' => 'index',
        'controller' => 'index',
        'edit' => 0,
    ],
    [
        'name' => 'Текстовые страницы',
        'module' => 'content',
        'controller' => 'content',
    ],
    [
        'name' => 'Системные страницы',
        'module' => 'content',
        'controller' => 'control',
    ],
    [
        'name' => 'Новости',
        'module' => 'content',
        'controller' => 'news',
    ],
    [
        'name' => 'Статьи',
        'module' => 'content',
        'controller' => 'articles',
    ],
    [
        'name' => 'Меню',
        'module' => 'menu',
        'controller' => 'menu',
    ],
    [
        'name' => 'Слайдшоу',
        'module' => 'multimedia',
        'controller' => 'slider',
    ],
    [
        'name' => 'Галерея',
        'module' => 'multimedia',
        'controller' => 'gallery',
    ],
    [
        'name' => 'Банерная система',
        'module' => 'multimedia',
        'controller' => 'banners',
    ],
       [
        'name' => 'Группы товаров',
        'module' => 'catalog',
        'controller' => 'groups',
    ],
    [
        'name' => 'Товары',
        'module' => 'catalog',
        'controller' => 'items',
    ],
    [
        'name' => 'Производители',
        'module' => 'catalog',
        'controller' => 'brands',
    ],
    [
        'name' => 'Модели',
        'module' => 'catalog',
        'controller' => 'models',
    ],
    [
        'name' => 'Спецификации',
        'module' => 'catalog',
        'controller' => 'specifications',
    ],
    [
        'name' => 'Пользователи сайта',
        'module' => 'user',
        'controller' => 'users',
    ],
    [
        'name' => 'Сообщения из контактной формы',
        'module' => 'contacts',
        'controller' => 'contacts',
        'view' => false,
    ],
    [
        'name' => 'Настройки сайта',
        'module' => 'config',
        'controller' => 'config',
    ],
    [
        'name' => 'Продукция',
        'module' => 'products',
        'controller' => 'products'
    ],
    [
        'name' => 'Шаблоны писем',
        'module' => 'mailTemplates',
        'controller' => 'mailTemplates',
        'view' => false,
    ],
];