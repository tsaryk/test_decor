<?php

return [
    'frontend' => [
        'Ajax', 'Index', 'Articles', 'News', 'User', 'Sitemap', 'Contact', 'Content', 'Services', 'Portfolio', 'Products'
    ],
    'backend' => [
        'Ajax', 'Index', 'Catalog', 'Config', 'Contacts', 'Content', 'Index', 'Log', 'Visitors', 'Blog', 'Blacklist',
        'MailTemplates', 'Menu', 'Orders', 'Seo', 'Subscribe', 'User', 'Multimedia', 'Statistic', 'Reviews', 'Crop',
        'Translates'
//        'Ajax', 'Index', 'Config', 'Contacts', 'Content', 'Index', 'Menu', 'User', 'MailTemplates', 'Multimedia', 'Crop',
//        'Services', 'Portfolio', 'Products', 'Seo',
    ],
];