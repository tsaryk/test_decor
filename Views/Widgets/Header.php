<?php
use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<header class="header">
    <div class="header__top">
        <div class="header__top-logo">
            <a href="<?php echo HTML::link(); ?>" class="">
                <svg>
                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg?v1490021372177#logo');?>" />
                </svg>
            </a>
        </div>
        <ul class="header__contact-list">
            <li class="header__contact-item">
                <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.phone')); ?>" class="header__contact-link"><?php echo Config::get('static.phone'); ?></a>
            </li>
            <li class="header__contact-item">
                <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.second_phone')); ?>" class="header__contact-link"><?php echo Config::get('static.second_phone'); ?></a>
            </li>
        </ul>
        <button class="header__button js-menuButton">
            <span></span>
        </button>
    </div>
    <div class="header__wrap">
        <div class="header__logo">
            <a href="<?php echo HTML::link(); ?>" class="header__logo-link">
                <svg>
                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg?v1490021372177#logo');?>" />
                </svg>
            </a>
        </div>
        <div class="header__menu">
            <ul class="header__menu-list js-menu">
                <?php echo Widgets::get('Header_MenuItem', ['result' => $contentMenu, 'cur' => 0])?>
            </ul>
        </div>
        <div class="header__contact">
            <ul class="header__contact-list">
                <li class="header__contact-item">
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.phone')); ?>" class="header__contact-link"><?php echo Config::get('static.phone'); ?></a>
                </li>
                <li class="header__contact-item">
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.second_phone')); ?>" class="header__contact-link"><?php echo Config::get('static.second_phone'); ?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="header__mobile">
        <div class="header__mobile-head">
            <div class="header__mobile-logo">
                <a href="<?php echo HTML::link(); ?>" class="header__logo-link">
                    <svg>
                        <use xlink:href="<?php echo HTML::media('/svg/sprite.svg');?>" />
                    </svg>
                </a>
            </div>
            <div class="header__mobile-contact">
                <ul class="header__contact-list">
                    <li class="header__contact-item">
                        <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.phone')); ?>" class="header__contact-link"><?php echo Config::get('static.phone'); ?></a>
                    </li>
                    <li class="header__contact-item">
                        <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.second_phone')); ?>" class="header__contact-link"><?php echo Config::get('static.second_phone'); ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="header__mobile-body">
            <ul class="header__menu-list js-menu">
                <?php foreach ($contentMenu as $obj): ?>
                    <li><a href="<?php echo HTML::link($obj->url); ?>"><?php echo $obj->name; ?></a></li>

                    <li class="header__menu-item">
                        <div class="header__menu-content">
                            <a href="<?php echo HTML::link($obj->url); ?>" class="header__menu-link">
                                <span><?php echo $obj->name; ?></span>
                            </a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</header>