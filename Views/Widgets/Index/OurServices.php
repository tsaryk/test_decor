<?php use Core\HTML;

if(isset($services) && !empty($services)): foreach ($services as $service): ?>
    <?php if(is_file(HOST.HTML::media('images/services/'.$service->image, false))):?>
    <section class="section__services" style="background-image: url(<?php echo HTML::media('images/services/'.$service->image);?>">
        <?php else:?>
    <section class="section__services" style="background-image">
        <?php endif;?>
        <div class="section__container">
            <div class="section__txtBlock">
                <h3 class="section__txtBlock-title"><?php echo $service->name;?></h3>
                <div class="section__txtBlock-text"><?php echo $service->text;?></div>
                <div class="section__txtBlock-link">
                    <a href="<?php echo $service->alias;?>" class="link link--detail">подробнее</a>
                </div>
            </div>
        </div>
    </section>
<?php endforeach; endif;?>
