<section class="section__countUp">
    <div class="section__container">
        <div class="section__countUp-wrap js-countUpWrap">
            <div class="section__countUp-column">
                <div class="section__countUp-item">
                    <span><?php use Core\Config;

                        echo $config['left_top'];?></span>
                    <div class="section__countUp-num js-countUp" data-max="<?php echo $config['left_center'];?>"></div>
                    <span><?php echo $config['left_down'];?></span>
                </div>
                <div class="section__countUp-shadow js-countUp" data-max="<?php echo $config['left_center'];?>"></div>
            </div>
            <div class="section__countUp-column">
                <div class="section__countUp-item">
                    <span><?php echo $config['left_top'];?></span>
                    <div class="section__countUp-num js-countUp" data-max="<?php echo $config['center_block'];?>"></div>
                    <span><?php echo $config['left_down'];?></span>
                </div>
                <div class="section__countUp-shadow js-countUp" data-max="<?php echo $config['center_block'];?>"></div>
            </div>
            <div class="section__countUp-column">
                <div class="section__countUp-item">
                    <span><?php echo $config['right_top'];?></span>
                    <div class="section__countUp-num js-countUp" data-max="<?php echo $config['right_block'];?>"></div>
                    <span><?php echo $config['right_down'];?></span>
                </div>
                <div class="section__countUp-shadow js-countUp" data-max="<?php echo $config['right_block'];?>"></div>
            </div>
        </div>
    </div>
</section>