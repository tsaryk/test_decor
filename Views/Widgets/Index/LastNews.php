<?php use Core\Dates;
use Core\HTML;
use Core\Text; ?>
<?php if(isset($result) && !empty($result)):?>
<section class="section__news">
    <div class="section__container">
        <div class="title title--main">
            <span>последние новости</span>
        </div>
        <div class="section__news-wrap">
            <ul class="section__news-list">
                <?php foreach($result as $obj):?>
                    <li class="section__news-item">
                        <a href="<?php echo HTML::link("/news/{$obj->alias}");?>" class="section__news-title"><?php echo $obj->name;?></a>
                        <span class="section__news-date">
                            <?php echo date("d", $obj->created_at)."&nbsp;".Dates::month(date("m", $obj->created_at));?>
                        </span>
                        <div class="section__news-text"><?php echo Text::limit_words(strip_tags($obj->text), 250);?></div>
                        <a href="<?php echo HTML::link("/news/{$obj->alias}");?>" class="link link--detail">подробнее</a>
                    </li>
                <? endforeach;?>
            </ul>
            <div class="section__news-more">
                <a href="<?php echo HTML::link('/news');?>" class="but"><span>читать все новости</span></a>
            </div>
        </div>
    </div>
</section>
<?php endif;?>