<?php
use Core\HTML;?>
<?php if(isset($portfolio) && !empty($portfolio)):?>
<section class="section__work" id="work">
    <div class="section__container">
        <div class="title title--main">
            <span>наши работы</span>
        </div>
        <div class="section__work-content">
            <?php
            $counter = 1;
            foreach($portfolio as $obj):
                if($counter == 2):?>
                    <div class="section__work-item section__work-item--type2">
                        <div class="section__work-img section__work-img--type2">
                            <?php if(!empty($obj->image)):?>
                                <img src="<?php echo HTML::media("images/gallery/".$obj->image);?>" alt="">
                            <?php endif;?>
                        </div>
                        <div class="section__work-wrap">
									    <span class="section__work-text">
                                             <span class="section__work-title"><?php echo $obj->name;?></span>
                                             <a href="portfolio/<?php echo $obj->alias;?>" class="link link--detail">подробнее</a>
                                         </span>
                        </div>
                    </div>
                    <?php $counter = 1;
                else:?>
                    <div class="section__work-item">
                        <div class="section__work-wrap">
									<span class="section__work-text">
                                             <span class="section__work-title"><?php echo $obj->name;?></span>
                                             <a href="portfolio/<?php echo $obj->alias;?>" class="link link--detail">подробнее</a>
                                         </span>
                        </div>
                        <div class="section__work-img section__work-img--type1">
                            <?php if(!empty($obj->image)):?>
                                <img src="<?php echo HTML::media("images/gallery/".$obj->image);?>" alt="">
                            <?php endif;?>
                        </div>
                    </div>


                    <?php $counter++;
                endif;
            endforeach;?>
        </div>
    </div>
</section>
<?php endif;?>