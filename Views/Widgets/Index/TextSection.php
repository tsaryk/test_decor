<?php if(isset($result) && !empty($result)):?>
    <section class="section__detail">
        <div class="section__container">
            <div class="view-text">
                <h3><?php echo $result->title;?></h3>
                <p><?php echo $result->text;?></p>
            </div>
        </div>
    </section>
<?php endif;?>