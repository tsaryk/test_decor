<?php
use Core\HTML;
use Core\View;
use Core\Config;
?>

<div class="fade js-fade">
    <ul class="fade__list">
        <?php foreach ($result as $obj): ?>
            <li class="fade__item">
                <div class="fade__content">
                    <div class="fade__img" style="background-image: url('<?php echo HTML::media('images/slider/big/' . $obj->image); ?>')"></div>
                    <?php if(!empty($obj->text) || !empty($obj->name)):?>
                        <div class="fade__text">
                            <div class="slider__size">
                                <div class="section__txtBlock">
                                    <h3 class="section__txtBlock-title"><?php echo $obj->name;?></h3>
                                    <div class="section__txtBlock-text"><?php echo $obj->text;?></div>
                                    <div class="section__txtBlock-link">
                                        <?php if(!empty($obj->url)):?>
                                        <a href="<?php echo $obj->url;?>" class="link link--detail">подробнее</a>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </li>
        <?php endforeach;?>
    </ul>
    <ul class="fade__pagination"></ul>
    <div class="slider__footer">
        <div class="slider__size">
            <div class="slider__footer-item">
                <a href="mailto:<?php echo Config::get('static.footer_email');?>" class="slider__footer-mail"><?php echo Config::get('static.footer_email');?></a>
            </div>
            <div class="slider__footer-item slider__footer-item--anchor">
                <a href="#work" class="slider__footer-anchor js-blockAncher">дальше</a>
            </div>
            <div class="slider__footer-item">
                <a href="<?php echo Config::get('socials.fb');?>" class="slider__footer-social">
                    <span>присоединяйтесь</span>
                    <svg>
                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg?v1490021372177#facebook');?>" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>
