<?php
use Core\HTML;
use Core\Widgets;

if(isset($result[$cur]) && !empty($result[$cur])):?>
    <ul class="header__menuSub">
        <?php foreach ($result[$cur] as $item):?>
            <li class="header__menuSub-item">
                <a href="<?php echo HTML::link("{$parent_alias}{$item->url}"); ?>" class="header__menuSub-link">
                    <span><?php echo $item->name;?></span>
                </a>
                <?php echo Core\View::tpl([
                        'result' => $result,
                        'tpl_folder' => $tpl_folder,
                        'cur' => $item->id,
                        'parent_alias' => "{$parent_alias}{$item->url}"
                ], 'Widgets/Header/SubMenuItem'); ?>
            </li>
        <?php endforeach;?>
    </ul>
<?php endif;?>