<?php use Core\HTML;

if (isset($result[$cur]) AND count($result[$cur]) ): ?>
    <?php if ($cur > 0): ?>
        <ol>
    <?php endif ?>
    <?php foreach ($result[$cur] as $obj): ?>
            <li><a href="<?php echo HTML::link("{$obj->url}"); ?>"><?php echo $obj->name; ?></a></li>

                <li class="header__menu-item">
                    <div class="header__menu-content">
                        <a href="<?php echo HTML::link($obj->url); ?>" class="header__menu-link">
                            <span><?php echo $obj->name; ?></span>
                        </a>
                        <?php echo Core\View::tpl([
                                'result' => $result,
                                'cur' => $obj->id,
                                'parent_alias' => $obj->url
                        ], 'Widgets/Header/SubMenuItem'); ?>
                    </div>
            </li>
    <?php endforeach; ?>
    <?php if ($cur > 0): ?>
        </ol>
    <?php endif ?>
<?php endif ?>