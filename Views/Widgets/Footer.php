<?php
use Core\Widgets;
use Core\HTML;
use Core\Config;
?>
<footer class="footer">
    <div class="footer__logo">
        <div class="footer__logo-wrap">
            <a href="#" class="footer__logo-link">
                <svg>
                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg?v1490021372177#logo');?>" />
                </svg>
            </a>
            <span class="footer__logo-text"><?php if(isset($footerData['copyright']) && !empty($footerData['copyright'])): echo $footerData['copyright']; endif;?>
        </div>
    </div>
    <div class="footer__content">
        <h3 class="footer__title">Давайте сотрудничать</h3>
        <div class="footer__column">
            <h3 class="footer__column-title">звоните</h3>
            <ul class="footer__tel">
                <?php if(isset($footerData['firstPhone']) && !empty($footerData['firstPhone'])):?>
                    <li class="footer__tel-item">
                        <a href="tel:<?php echo preg_replace('/\s+|,+/', '', $footerData['firstPhone']); ?>"
                           class="header__contact-link"><?php echo $footerData['firstPhone']; ?></a>
                    </li>
                <?php endif;?>
                <?php if(isset($footerData['secondPhone']) && !empty($footerData['secondPhone'])):?>
                <li class="footer__tel-item">
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', $footerData['secondPhone']); ?>"
                       class="header__contact-link"><?php echo $footerData['secondPhone']; ?></a>
                </li>
                <?php endif;?>
                <?php if(isset($footerData['thrirdPhone']) && !empty($footerData['thrirdPhone'])):?>
                <li class="footer__tel-item">
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', $footerData['thrirdPhone']); ?>"
                       class="header__contact-link"><?php echo $footerData['thrirdPhone']; ?></a>
                </li>
                <?php endif;?>
            </ul>
        </div>
        <?php if(isset($footerData['email']) && !empty($footerData['email'])):?>
        <div class="footer__column">
            <h3 class="footer__column-title">Пишите</h3>
            <a href="mailto:<?php echo $footerData['email']; ?>" target="_blank" class=footer__link><?php echo $footerData['email']; ?></a>
        </div>
        <?php endif;?>
        <?php if(isset($footerData['address']) && !empty($footerData['address'])):?>
        <div class="footer__column">
            <h3 class="footer__column-title">Приходите</h3>
            <span class="footer__link"><?php echo $footerData['address']; ?></span>
        </div>
        <?php endif;?>
        <div class="footer__column">
            <h3 class="footer__column-title">присоединяйтесь</h3>
            <?if(!empty($footerData['vk'])): ?>
                <a href="<?php echo $footerData['vk'];?>" target="_blank" class="footer__link">VK</a>
            <?endif;?>
            <?if(!empty($footerData['fb'])): ?>
                <a href="<?php echo Config::get('socials.fb');?>" target="_blank" class="footer__link">Facebook</a>
            <?endif;?>
            <?if(!empty($footerData['instagram'])): ?>
                <a href="<?php echo Config::get('socials.instagram');?>" target="_blank" class="footer__link">Instagram</a>
            <?endif;?>
        </div>
        <div class="footer__column footer__column--anchor">
            <a href="<?php echo HTML::link();?>" class="footer__author">
                <img src="<?php echo HTML::media('css/pic/wezom_logo.png');?>" alt="">
            </a>
        </div>
    </div>
</footer>
<div class="view-dim js-dim"></div>
<div class="section__anchor js-pageAncher">
			<span>
				<img src="<?php echo HTML::media('css/pic/anchor.png');?>" alt="">
			</span>
</div>
<ul class="view-line">
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
    <li>
        <span></span>
    </li>
</ul>
</div>
<link rel="stylesheet" href="<?php echo HTML::media('css/vendor/jquery-plugins.css?v1490021372177');?>">
<link rel="stylesheet" href="<?php echo HTML::media('css/style.css?v1490021372177');?>">
<!--<script src="--><?php //echo HTML::media('js/jquery-1.11.0.min.js');?><!--"></script>-->
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js/media"></script><![endif]-->
<?php $js = Minify\Core::factory('js')->minify($scripts); ?>
<?php foreach ($js as $file_script): ?>
    <?php echo HTML::script($file_script) . "\n"; ?>
<?php endforeach; ?>

<?php foreach ($scripts_no_minify as $file_script): ?>
    <?php echo HTML::script($file_script) . "\n"; ?>
<?php endforeach; ?>

<script>"use strict";!function(win){function testLocal(testKey){try{return localStorage.setItem(testKey,testKey),localStorage.removeItem(testKey),!0}catch(e){return!1}}win.localSupport=testLocal("test-key"),win.localWrite=function(key,val){try{localStorage.setItem(key,val)}catch(e){if(e==QUOTA_EXCEEDED_ERR)return!1}}}(window);</script>

<script>var MEDIA_FONT_PATH = '';</script>

<script>"use strict";!function(win){function writeCSS(styleString){var styleElement=document.createElement("style");styleElement.rel="stylesheet",document.getElementsByTagName("head")[0].appendChild(styleElement);try{styleElement.innerText=styleString,styleElement.innerHTML=styleString}catch(e){try{styleElement.styleSheet.cssText=styleString}catch(e){return}}}function supportsWoff2(){if(!win.FontFace)return!1;var woff2=new FontFace("t",'url(data:application/font-woff2;charset=utf-8;base64,d09GMgABAAAAAAPMAAsAAAAACbAAAAOBAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlYAhU4RCAooQgsIAAE2AiQDDAQgBYlfBykb+wjILgpsY9rIRchcpL+zqLG4GAoeCOXBvxxKS1CtkT13+88BREUkEwWIOopZJQqQXCRyeQL3YNCcftYasfGNOEnLagBkPwT/RVJmaopA6MkZASgX/1jgmfvWsYADwGXMg//19xpvNumUeUBplAn4nyj92cyjTe/6AVNxAxhMKKKxRh0w15rojR/gERzbLehZ+RZSQCN7MKmAXCGS2KOA9Dg9Jm8ojdFWt5wUhBTPxuEPIHNI679wQZWBCP9AHFwDQIYc9QUE6gtmdzAIBDXXdOG4cy675a4nXvvopwohDmNl7MseZe+zj63roxO9GMAIRCSxhJtqcMhxJ11wzR0PPfPWF79DGg+i66Ed3YArsVz1VJgpfdCfULWlilUmKztWtkeAL++/POXLvS/jCAaVRGJIrMWImBCTYmWsi21xBuIeAHEtbsSdeAD88WFWVBuGORRCFilGKSbO1JdIZSV1ilmNovI1r71euv/thbdAINj3eZ7ZoP/FZcXg5bBR08y5Z6RFvia9RrEMBOPrpgnkawQAIE+sgQSBPORywH5z574FoBciQMj0IoBMkT4EkKtlMAEkOFKgpy1Z6LS/1FAVAZFymXK3NShQtb1wIYqvsAjfuBjF/9gSLCoiIcXiSKZmLI/kWlaVCY4UmNYBWYiFSq3qvyzNl63Mt6wsR6GmKs/WQ21VM9sN9VTdncv6frlBHHhvbMhNwuFDbgbjq7GFbIVxe7/4iXvgtq+yOL+yOG5Z7qTKy9HSQzucjcvWY8uOXgHy4zN/Q6Y3VG0rl30bfMmTX1lnyqnkAeqCNCOKbAbLaiE+FYt+Z51e8xIrNK230/usiXWRPsKvr6asR2ciB6l0xSpP33hMy+gPkx1cho/ycIpyNcznYP6scD70UA7FaKgM7RzML+f384d+hdv5nfycccpSdAZKpYNLrY0p4/IyQMX5UiimbNwMkIkkUfyUeR4PHLCZLDlZer8uS5dRoNN4ZKtlyvPyQUIj6QT+flk2jgHJDJHoxCg1xdfwKfgqxE3lh7SajQk5pvkazNB5dBQ/7YjFlgUGjsmBorMFqowfyFkayON+xkyt+MwswiYGGYhyJKZABuen1uqhNm2LgMmmLqi4ViM6Yxvn3yxr0RkdY7QEUUusuS2TlDbTsDS42f6xPDyj20EVUBqGm5eRkcfkUG1A1XbzEAEAIJ9+FhkA) format("woff2")',{});return woff2.load().catch(function(){}),"loading"===woff2.status||"loaded"===woff2.status}function loadExternalFont(url,urlKey,cssKey){var request=new XMLHttpRequest;request.open("GET",url),request.onload=function(){if(request.status>=200&&request.status<400){var css=request.responseText;writeCSS(css),win.localSupport&&(win.localWrite(urlKey,url),win.localWrite(cssKey,css))}else console.warn("request loadExternalFont - fail")},request.send()}function loadFont(fontName,srcWoff,srcWoff2){var prefix=fontName+"-x-font-",urlKey=prefix+"url",cssKey=prefix+"css",url=srcWoff2&&supportsWoff2()?srcWoff2:srcWoff;if(win.localSupport){var localUrl=localStorage[urlKey],localCss=localStorage[cssKey];localCss&&localUrl===url?writeCSS(localCss):loadExternalFont(url,urlKey,cssKey)}else loadExternalFont(url,urlKey,cssKey)}loadFont("Font-name",MEDIA_FONT_PATH+"css/fonts/fonts-woff.css",MEDIA_FONT_PATH+"css/fonts/fonts-woff2.css")}(window);</script>

<script>!function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.svg4everybody=b()}):"object"==typeof exports?module.exports=b():a.svg4everybody=b()}(this,function(){function a(a,b){if(b){var c=document.createDocumentFragment(),d=!a.getAttribute("viewBox")&&b.getAttribute("viewBox");d&&a.setAttribute("viewBox",d);for(var e=b.cloneNode(!0);e.childNodes.length;)c.appendChild(e.firstChild);a.appendChild(c)}}function b(b){b.onreadystatechange=function(){if(4===b.readyState){var c=b._cachedDocument;c||(c=b._cachedDocument=document.implementation.createHTMLDocument(""),c.body.innerHTML=b.responseText,b._cachedTarget={}),b._embeds.splice(0).map(function(d){var e=b._cachedTarget[d.id];e||(e=b._cachedTarget[d.id]=c.getElementById(d.id)),a(d.svg,e)})}},b.onreadystatechange()}function c(c){function d(){for(var c=0;c<l.length;){var g=l[c],h=g.parentNode;if(h&&/svg/i.test(h.nodeName)){var i=g.getAttribute("xlink:href");if(e&&(!f.validate||f.validate(i,h,g))){h.removeChild(g);var m=i.split("#"),n=m.shift(),o=m.join("#");if(n.length){var p=j[n];p||(p=j[n]=new XMLHttpRequest,p.open("GET",n),p.send(),p._embeds=[]),p._embeds.push({svg:h,id:o}),b(p)}else a(h,document.getElementById(o))}}else++c}k(d,67)}var e,f=Object(c),g=/\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,h=/\bAppleWebKit\/(\d+)\b/,i=/\bEdge\/12\.(\d+)\b/;e="polyfill"in f?f.polyfill:g.test(navigator.userAgent)||(navigator.userAgent.match(i)||[])[1]<10547||(navigator.userAgent.match(h)||[])[1]<537;var j={},k=window.requestAnimationFrame||setTimeout,l=document.getElementsByTagName("use");e&&d()}return c});</script>
<script>document.addEventListener("DOMContentLoaded", function() { svg4everybody({}); });</script>
<!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js?v1490021372177"></script><![endif]-->


<script src="<?php echo HTML::media('js/vendor/modernizr.js?v1490021372177');?>"></script>
<script src="<?php echo HTML::media('js/vendor/jquery.js?v1490021372177');?>"></script>
<script src="<?php echo HTML::media('js/vendor/jquery-plugins.js?v1490021372177');?>"></script>
<script src="<?php echo HTML::media('js/init.js?v1490021372177');?>"></script>

<script src="<?php echo HTML::media('js/programmer/translate-ru.js?v1490021372177');?>"></script>
<!--wold.js settings -->

<script>var $wzmOld_URL_IMG = 'Media/css/pic/wezom-info-red.gif'</script>
<script async="async" defer="defer" src="<?php echo HTML::media('js/wold.js?v1490021372177');?>"></script>

<noscript>
    <link rel="stylesheet" href="<?php echo HTML::media('css/noscript-msg.css?v1490021372177');?>">
    <input id="noscript-msg__close" type="checkbox" title="Закрити">
    <div id="noscript-msg">
        <label id="noscript-msg__times" for="noscript-msg__close" title="Закрити">&times;</label>
        <a href="http://wezom.com.ua/" target="_blank" title="Cтудія Wezom" id="noscript-msg__link">&nbsp;</a>
        <div id="noscript-msg__block">
            <div id="noscript-msg__text">
                <p>В Вашем браузере отключен
                    <strong>JavaScript</strong>! Для корректной работы с сайтом необходима поддержка Javascript.</p>
                <p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
            </div>
        </div>
    </div>
</noscript>