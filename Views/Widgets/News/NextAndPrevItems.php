<?php use Core\HTML;

if(isset($prevItem) || isset($nextItem)): ?>
    <div class="pagination__nav">
        <?php if(isset($prevItem)):?>
            <a href="<?php echo HTML::link('news/'.$prevItem->alias);?>" class="pagination__nav-link pagination__nav-link--prev">
                <span>предыдущая новость</span>
            </a>
        <?php endif;?>
        <?php if(isset($nextItem)):?>
            <a href="<?php echo HTML::link('news/'.$nextItem->alias);?>" class="pagination__nav-link pagination__nav-link--next">
                <span>Следующая новость</span>
            </a>
        <?php endif;?>
    </div>

<?php endif;?>