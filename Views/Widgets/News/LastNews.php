<?php use Core\HTML;

if(isset($result) && !empty($result)):?>
    <div class="news__last">
        <h2 class="news__last-title">другие новости</h2>
        <ul class="section__news-list">
            <?php foreach($result as $item):?>
                <li class="section__news-item">
                    <a href="" class="section__news-title"><?php echo $item->name; ?></a>
                    <span class="section__news-date">
                        <?php if ($item->date): ?>
                            <?php echo date('d.m.Y', $item->date); ?>
                        <?php endif; ?>
                    </span>
                    <div class="section__news-text"><?php echo $item->text;?></div>
                    <a href="<?php echo HTML::link('news')."/{$item->alias}"; ?>" class="link link--detail">подробнее</a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>