<?php
use Core\Widgets;
use Core\Arr;
use Core\HTML;
use Core\Config;
use Core\Dates;
?>
<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body>
<div class="view-wrapper">
    <?php echo Widgets::get('Header');?>
    <div class="view-continer">
        <main class="section">
            <section class="contact">
                <div class="contact__wrap">
                    <div class="contact__head">
                        <h2 class="title title--contact"><?php echo Arr::get($_seo, 'h1');?></h2>
                        <div class="breadcrumbs">
							<?php echo $_breadcrumbs;?>
                        </div>
                    </div>
                    <?php echo $_content;?>
                </div>
            </section>
        </main>
    </div>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config]); ?>
</body>
</html>