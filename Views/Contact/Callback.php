<div class="hidden-wrapper hidden-wrapper--md">
	<div class="contact__form">
        <h2 class="title title--contact">обратная связь</h2>
        <form class="form js-form" data-form="true" data-ajax="contacts" id="contactForm" enctype="multipart/form-data">
            <div class="form__group">
                <div class="form__input control-holder control-holder--text">
                    <input required type="text" data-name="name" name="name" data-rule-minlength="2" data-rule-word="true" placeholder="ваше имя">
                </div>
                <div class="form__input control-holder control-holder--text">
                    <input class="js-inputmask" required type="tel" data-name="phone" name="phone" placeholder="+38(___)___-__-__" data-mask="+38(099)9999999">
                </div>
                <div class="form__input control-holder control-holder--text">
                    <input required type="text" data-name="text" name="text" data-rule-minlength="10" placeholder="сообщение">
                </div>
                <?php if (array_key_exists('token', $_SESSION)): ?>
                    <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>"/>
                <?php endif; ?>
            </div>
            <button type="submit" class="but js-form-submit" id="sendContactForm">
                <span>Отправка формы</span>
            </button>
        </form>
	</div>
</div>
