<?php
use Core\Config;
use Core\HTML;
?>
<section class="contact">
    <div class="contact__wrap">
        <div class="contact__map js-map" data-lat="46.647792" data-lng="32.588002" data-zoom="18" data-marker="<?php HTML::media('css/pic/marker.png');?>" data-mapApi="<?php echo Config::get('static.contact_map');?>">
        </div>
        <div class="contact__content">
            <div class="contact__content-wrap">
                <div class="contact__block">
                    <h4 class="contact__title">адрес</h4>
                    <span class="contact__text"><?php echo Config::get('static.footer_address');?></span>
                </div>
                <div class="contact__block">
                    <h4 class="contact__title">телефоны</h4>
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.phone')); ?>" class="contact__text contact__text--up"><?php echo Config::get('static.phone');?> </a>
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.second_phone')); ?>" class="contact__text contact__text--up"><?php echo Config::get('static.second_phone');?> </a>
                    <a href="tel:<?php echo preg_replace('/\s+|,+/', '', Config::get('static.thrird_phone')); ?>" class="contact__text contact__text--up"><?php echo Config::get('static.thrird_phone');?></a>
                </div>
                <div class="contact__block">
                    <h4 class="contact__title">электронный адрес</h4>
                    <a href="mailto:<?php echo Config::get('static.footer_email');?>" target="_blank" class="contact__text"><?php echo Config::get('static.footer_email');?></a>
                </div>
                <div class="contact__block">
                    <h4 class="contact__title">Мы в соц.сетях</h4>
                    <a href="<?php echo Config::get('socials.fb');?>" target="_blank" class="contact__text  contact__text--facebook">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg?v1490021372177#facebook');?>">
                        </svg>
                        <span>Facebook</span>
                    </a>
                </div>
                <button class="but js-mfp-ajax" data-url="<?php echo HTML::link('contact/callback');?>">
                    <span>обратная связь</span>
                </button>
            </div>
        </div>
    </div>
</section>