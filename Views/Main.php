<?php
use Core\Widgets;
use Core\Arr;
use Core\HTML;
use Core\Config;
use Core\Dates;
?>
<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body>
<div class="view-wrapper">
    <?php echo Widgets::get('Header');?>
    <div class="view-continer">
        <main class="section">
            <?php echo Widgets::get('Index_Slider'); ?>
            <!--section out works-->
            <?php echo Widgets::get('Index_OurWorks');?>
            <!--section our services-->
            <?php echo Widgets::get('Index_OurServices');?>
            <!--section achievements-->
            <?php echo Widgets::get('Index_OurAchievements');?>
            <!--section last news-->
            <?php echo Widgets::get('Index_LastNews');?>
            <!--section seo-->
            <?php echo Widgets::get('Index_TextSection');?>
        </main>
    </div>
<!--    --><?php //echo Widgets::get('HiddenData'); ?>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config]); ?>
</body>
</html>

