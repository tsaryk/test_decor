<?php
use Core\HTML;
use Core\Text;
?>
<?php if(isset($systemPage) && !empty($systemPage)): echo $systemPage->text; endif;?>
    <section class="news">
        <div class="news__head">
            <?php echo \Core\Widgets::get('GetBreadCrumbs', $breadcrumbs);?>
            <?php if(isset($result) && !empty($result)):
                $lastItem = current($result);?>
                <?php if (is_file(HOST.HTML::media('images/news/big/'.$lastItem->image ,false))):?>
                    <a href="<?php echo HTML::link('news/' . $lastItem->alias); ?>" class="news__head-wrap"
                       style="background-image: url(<?php echo HTML::media('images/news/big/' . $lastItem->image); ?>)">
                <?php else: ?>
                    <a href="<?php echo HTML::link('news/' . $lastItem->alias); ?>" class="news__head-wrap"
                       style="background-image: <?php echo HTML::media('images/no-image.png');?>">
                <?php endif;?>
                <span class="news__head-content">
                    <span class="news__text">
                        <span class="news__title"><?php echo $lastItem->name;?></span>
                        <span class="news__date">
                            <?php if ($lastItem->date): ?>
                                <?php echo date('d.m.Y', $lastItem->date); ?>
                            <?php endif; ?>
                        </span>
                        <span class="news_p"><?php echo Text::limit_words(strip_tags($lastItem->text), 100); ?></span>
                        <span class="link link--detail">подробнее</span>
                    </span>
                </span>
                    </a>
                <?php array_shift($result);
            endif; ?>
        </div>
        <div class="news__content">
            <div class="view-size">
                <ul class="news__list js-masonry">
                    <div class="grid-sizer"></div>
                    <?php if(isset($result) && !empty($result)):
                        foreach($result as $obj):?>
                            <li class="news__item">
                                <a href="<?php echo HTML::link('news/' . $obj->alias); ?>" class="news__img">
                                <?php if (is_file(HOST.HTML::media('images/news/big/'.$obj->image ,false))):?>
                                    <img src="<?php echo HTML::media('images/news/big/' . $obj->image); ?>" alt="">
                                <?php else:?>
                                    <img src="<?php echo HTML::media('images/no-image.png');?>" alt="">
                                <?php endif;?>
                                </a>
                                <a href="<?php echo HTML::link('news/' . $obj->alias); ?>" class="news__title"><?php echo $obj->name?></a>
                                <div class="news__date">
                                    <?php if ($obj->date): ?>
                                        <?php echo date('d.m.Y', $obj->date); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="news_p"><?php echo Text::limit_words(strip_tags($obj->text), 100); ?></div>
                                <a href="<?php echo HTML::link('news/' . $obj->alias); ?>" class="link link--detail">подробнее</a>
                            </li>
                        <?php endforeach;
                    endif;?>
                </ul>
            </div>
        </div>
    </section>
<?php echo $pager; ?>