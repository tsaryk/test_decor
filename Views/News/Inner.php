<?php use Core\Dates;
use Core\HTML;
use Core\Widgets; ?>
<section class="news__elem">
    <?php if(isset($obj) && !empty($obj)):?>
        <div class="news__head">
            <span class="news__head-date">
                <?php echo $obj->date; ?></span>
            <div class="news__head-img">
                <img src="<?php echo HTML::media('images/news/elem_head.jpg'); ?>" alt="">
            </div>
        </div>
        <div class="news__content ">
        <div class="view-text">
            <p><?php echo $obj->text;?></p>
        </div>
        <div class="news__footer">
            <div class="news__social">
                <img src="<?php echo HTML::media('css/pic/social.jpg?v1490021372177'); ?>" alt="">
            </div>
            <div class="pagination">
                <div class="pagination__wrap">
                    <?php echo $widgets['prevAndNextItems'];?>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
        <?php echo $widgets['lastNews'];?>
</section>