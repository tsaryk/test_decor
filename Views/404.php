<?php
use Core\Widgets;
use Core\HTML;
?>
<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body>
<div class="view-wrapper">
    <?php echo Widgets::get('Header');?>
    <div class="view-continer">
        <main class="section">
            <div class="view-continer">
                <div class="error">
                    <div class="error__head">
                        <h2 class="title title--contact">Страница не найдена</h2>
                        <div class="breadcrumbs">
						<span>
							<a href="<?php echo HTML::link();?>">
								Главная </a>
						</span>
                            <span>
							404 </span>
                        </div>

                    </div>
                    <div class="error__content">
                        <div class="error__text">
                            Возможно, страница, которую Вы пытаетесь открыть была удалена или не существует.
                            <br>Вы можете
                            <a href="<?php echo $_SERVER['HTTP_REFERER'];?>">вернуться назад</a> или перейти на
                            <a href="<?php echo HTML::link();?>">Главную страницу</a>.
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config]); ?>
</body>
</html>