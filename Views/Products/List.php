<?php use Core\HTML;
use Core\Widgets; ?>

<div class="view-size view-text">
<div class="products__head">
    <?php echo $_seo['description'];?>
</div>
<ul class="products__list">
    <?php foreach ($result as $obj): ?>
        <li class="products__item">
            <div class="products__text">
                <?php if(is_file(HOST.HTML::media('images/products/'.$obj->image, false))):?>
                <div class="products__img" style="background-image: url(<?php echo HTML::media('images/products/' . $obj->image); ?>)">
                    <?php else:?>
                        <div class="products__img" style="background-image">
                    <?php endif;?>
                        <span class="products__img-brand">
                            <?php if(is_file(HOST.HTML::media('images/products/'.$obj->logo_image, false))):?>
                                <img src="<?php echo HTML::media('images/products/' . $obj->logo_image); ?>" alt="">
                            <?php else:?>
                                <img src="<?php echo HTML::media('images/no-image.png');?>" alt="">
                            <?php endif;?>
                        </span>
                </div>
                <h3><?php echo $obj->name;?></h3>
                <p><?php echo $obj->text;?></p>
            </div>
        </li>
    <?php endforeach;?>
</ul>

