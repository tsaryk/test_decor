<?php
use Core\HTML;
use Core\Text;
use Core\Widgets;?>

<div class="about__container">
    <div class="about__head">
        <ul class="about__head-list">
            <?php echo $systemPage->text;?>
        </ul>
    </div>
    <div class="about__content">
        <h3 class="about__title"><?php echo $systemPage->title;?></h3>
        <div class="about__p"><?php echo $systemPage->description;?></div>
        <ul class="about__list">
            <?php if(isset($services) && !empty($services)):
                foreach($services as $service):?>
                    <li class="about__item">
                        <?php if(is_file(HOST.HTML::media('images/services/'.$service->image, false))):?>
                        <div class="about__top" style="background-image: url(<?php echo HTML::media('images/services/'.$service->image);?>)">
                            <?php else: ?>
                        <div class="about__top" style="background-image: <?php echo HTML::media('images/no-image.png');?>">
                            <?php endif;?>
                            <span><?php echo $service->name;?></span>
                        </div>
                        <div class="about__p"><?php echo $service->text;?></div>
                    </li>
                <?php endforeach;
            endif;?>
        </ul>
    </div>
</div>
