<?php
use Core\HTML;
use Core\Widgets; ?>
<div class="section__container">
<?php $counter = 1;
foreach($portfolio as $obj):
    if($counter == 2):?>
        <div class="section__work-item section__work-item--type2">
            <div class="section__work-img section__work-img--type2">
                <?php if(!empty($obj->image)):?>
                    <img src="<?php echo HTML::media("images/portfolio/".$obj->image);?>" alt="">
                <?php else:?>
                    <img src="<?php echo HTML::media('images/no-image.png');?>" alt="">
                <?php endif;?>
            </div>
            <div class="section__work-wrap">
			    <span class="section__work-text">
                     <span class="section__work-title"><?php echo $obj->name;?></span>
                     <a href="<?php echo HTML::link('portfolio/'.$obj->alias);?>" class="link link--detail">подробнее</a>
                </span>
            </div>
        </div>
        <?php $counter = 1;
    else:?>
        <div class="section__work-item">
            <div class="section__work-wrap">
						<span class="section__work-text">
                                 <span class="section__work-title"><?php echo $obj->name;?></span>
                                 <a href="<?php echo HTML::link('portfolio/'.$obj->alias);?>" class="link link--detail">подробнее</a>
                             </span>
            </div>
            <div class="section__work-img section__work-img--type1">
                <?php if(!empty($obj->image)):?>
                    <img src="<?php echo HTML::media("images/portfolio/".$obj->image);?>" alt="">
                <?php else:?>
                    <img src="<?php echo HTML::media('images/no-image.png');?>" alt="">
                <?php endif;?>
            </div>
        </div>


        <?php $counter++;
    endif;
endforeach;?>
</div>
