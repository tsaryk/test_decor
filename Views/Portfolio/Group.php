<?php  use Core\HTML;
use Core\Widgets;
?>

<div class="project__wrap">
    <ul class="project__list">
        <?php if(isset($result)):
            foreach($result as $obj):?>
                <li class="project__item">
                    <a href="<?php echo HTML::link("portfolio/".$group->alias."/".$obj->alias);?>" class="project__link">
								<span class="project__content">
									<span class="project__text">
										<span class="project__title"><?php echo $obj->name;?></span>
										<span class="project__p"><?php echo $obj->text;?></span>
                                        <span class="but">Узнать подробнее</span>
									</span>
								</span>
                        <span class="project__img">
                            <?php if(is_file(HOST.HTML::media('images/portfolio/'.$obj->image, false))):?>
                                <img src="<?php echo HTML::media('images/portfolio/'.$obj->image);?>" alt="">
                            <?php else:?>
                                <img src="<?php echo HTML::media('images/no-image.png');?>" alt="">
                            <?php endif;?>
                        </span>
                    </a>
                </li>
            <?php endforeach;
        endif;?>
    </ul>
</div>
