<?php use Core\HTML;
use Core\Widgets;?>
<div class="view-size">
    <div class="gallery__content">
        <div class="gallery__text view-text">
            <?php echo $result->text;?>
        </div>
        <?php if(isset($result->images) && !empty($result->images)):?>
            <ul class="gallery__list js-gallery">
                <?php foreach($result->images as $image):?>
                    <?php if(is_file(HOST.HTML::media('images/portfolio_images/'.$image->image, false))):?>
                        <li class="gallery__item">
                            <a href="<?php echo HTML::media('images/portfolio_images/'.$image->image);?>" class="gallery__img b-lazy"
                               data-src="<?php echo HTML::media('images/portfolio_images/'.$image->image);?>"></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach;?>
            </ul>
        <?php endif;?>
    </div>
</div>
