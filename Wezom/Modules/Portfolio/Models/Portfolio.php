<?php

namespace Wezom\Modules\Portfolio\Models;

use Core\Common;

class Portfolio extends Common
{
    public static $table = 'portfolio';
    public static $image = 'portfolio';

}