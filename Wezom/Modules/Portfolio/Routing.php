<?php

return [
    'wezom/portfolio/index' => 'portfolio/portfolio/index',
    'wezom/portfolio/edit/<id:[0-9]*>' => 'portfolio/portfolio/edit',
    'wezom/portfolio/add' => 'portfolio/portfolio/add',
    'wezom/portfolio/delete/<id:[0-9]*>' => 'portfolio/portfolio/delete',

];