<?php

return [
    'wezom/services/index' => 'services/services/index',
    'wezom/services/add' => 'services/services/add',
    'wezom/services/edit/<id:[0-9]*>' => 'services/services/edit',
    'wezom/services/delete/<id:[0-9]*>' => 'services/services/delete',
];