<?php
namespace Wezom\Modules\Products\Models;

class Products extends \Core\Common {

    public static $table = 'products';
    public static $image = 'products';
    public static $filters = [
        'name' => [
            'table' => NULL,
            'action' => 'LIKE',
        ],
    ];
    public static $rules = [];

    public static function valid($data = [])
    {
        static::$rules = [
            'name' => [
                [
                    'error' => __('Название продукта не может быть пустым!'),
                    'key' => 'not_empty',
                ],
            ],
        ];
        return parent::valid($data);
    }

}