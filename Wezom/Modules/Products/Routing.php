<?php

return [
    'wezom/products/index' => 'products/products/index',
    'wezom/products/edit/<id:[0-9]*>' => 'products/products/edit',
    'wezom/products/add' => 'products/products/add',
    'wezom/products/delete/<id:[0-9]*>' => 'products/products/delete',
    'wezom/products/delete_image/<id:[0-9]*>' => 'products/products/deleteImage',
];